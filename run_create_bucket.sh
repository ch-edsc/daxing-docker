#!/bin/bash

set -o allexport; source .env; set +o allexport
docker-compose up -d
pip3 install Minio
python3 ./create_bucket.py
