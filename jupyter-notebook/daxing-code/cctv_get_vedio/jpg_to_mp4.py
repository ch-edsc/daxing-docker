# -*- coding: utf-8 -*-

#from https://stackoverflow.com/questions/44947505/how-to-make-a-movie-out-of-images-in-python
import cv2
import os

image_folder = "data/img"
video_name = 'data/camera.mp4'

images = [img for img in os.listdir(image_folder) if img.endswith(".jpg")]
frame = cv2.imread(os.path.join(image_folder, images[0]))
height, width, layers = frame.shape

video = cv2.VideoWriter(video_name,  cv2.VideoWriter_fourcc(*'MP4V'), 7, (width,height))

#for image in images:
for i in range(7*60*10):
    video.write(cv2.imread(os.path.join(image_folder, str(i) + ".jpg")))

cv2.destroyAllWindows()
video.release()

print('end')