# -*- coding: utf-8 -*-

#from https://stackoverflow.com/questions/44947505/how-to-make-a-movie-out-of-images-in-python
import cv2
import os

#3 7 9 14
cameraNum = 14

#image_folder = "inference/output/"
image_folder = "inference/output_camera%d/" % cameraNum
#video_name = 'output/detect_by_yolo_pretrain_model.mp4'
video_name = 'inference/output_vedio/yolor_camera%d.mp4' % cameraNum
image_folder = "data/camera14/obj_train_data/"
video_name = 'data/camera14_10min.mp4'
images = [img for img in os.listdir(image_folder) if img.endswith((".png", ".PNG"))]
images.sort()

frame = cv2.imread(os.path.join(image_folder, images[0]))
height, width, layers = frame.shape

video = cv2.VideoWriter(video_name,  cv2.VideoWriter_fourcc(*'MP4V'), 7, (width,height))


for i in range(len(images)):
    #path = os.path.join(image_folder, "frame_%06d.png" % (i))
    path = os.path.join(image_folder, images[i])
    print("encode: " + path)
    video.write(cv2.imread(path))

cv2.destroyAllWindows()
video.release()

print('end')