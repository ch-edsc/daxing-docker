# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np

class CM:
    def __init__(self, classNums = 6, classNames = ["Bike","BigCar","SmallCar","MotorBike","Person","BackGround"], iouTh=0.5):
        self.confusion_matrix = []
        self.classNames = classNames
        self.classNums = classNums
        self.iouTh = iouTh
        for i in range(classNums):
            tempArray = []
            for j in range(classNums):
                tempArray.append(0)
            self.confusion_matrix.append(tempArray)
        return
            
    def add(self, predictArray, targetArray):
        #input 為二維陣列，每一欄內容是[classNumber, x1, y1, x2, y2]
        isGetT = np.zeros((len(targetArray)))
        isGetP = np.zeros((len(predictArray)))
                
        def getArea(tar):
            #算面積
            return (tar[3] - tar[1]) * (tar[4] - tar[2])
        def getOverlapArea(p, t):
            #算交集
            overlapW = max(min(p[3], t[3]) - max(p[1], t[1]), 0)
            overlapH = max(min(p[4], t[4]) - max(p[2], t[2]), 0)
            return overlapW * overlapH
        def getIou(p, t):
            #算iou，也就是交集/聯集，也就是交集/(A面積+B面積-交集)
            overlapArea = getOverlapArea(p, t)
            if overlapArea == 0:
                return 0
            else:
                return overlapArea / (getArea(p) + getArea(t) - overlapArea)
        #要考慮多個框框重疊在同一區域，先計算類別正確，再計算類別錯誤
        for pi in range(len(predictArray)):
            #計算類別正確
            for ti in range(len(targetArray)):
                if isGetT[ti] == 1:
                    continue
                p = predictArray[pi]
                t = targetArray[ti]
                iou = getIou(p, t)
                if iou >= self.iouTh:
                    if int(p[0]) < self.classNums and int(t[0]) < self.classNums:
                        if p[0] == t[0]:
                            self.confusion_matrix[int(t[0])][int(p[0])] += 1
                            isGetT[ti] = 1
                            isGetP[pi] = 1
            #計算類別錯誤
            for ti in range(len(targetArray)):
                if isGetT[ti] == 1:
                    continue
                p = predictArray[pi]
                t = targetArray[ti]
                iou = getIou(p, t)
                if iou >= self.iouTh:
                    if int(p[0]) < self.classNums and int(t[0]) < self.classNums:
                        self.confusion_matrix[int(t[0])][int(p[0])] += 1
                        isGetT[ti] = 1
                        isGetP[pi] = 1
        #然後計算漏抓與多抓，多出的predictArray就是多抓
        for pi in range(len(predictArray)):
            if isGetP[pi] == 0:
                if int(predictArray[pi][0]) < self.classNums:
                    self.confusion_matrix[self.classNums - 1][int(predictArray[pi][0])] += 1
        #多出的targetArray就是漏抓
        for ti in range(len(targetArray)):
            if isGetT[ti] == 0:
                if int(targetArray[ti][0]) < self.classNums:
                    self.confusion_matrix[int(targetArray[ti][0])][self.classNums - 1] += 1
        return
        
    def output(self):
        return self.confusion_matrix
    
    def draw(self, savepath = None):
        #避免其他程式碼也使用plt繪圖造成影響，先清空一輪
        plt.close()
        plt.figure(figsize=(8, 6))
        plt.axis('off')
        total_confusion_matrix = np.array(self.confusion_matrix)
        plt.matshow(total_confusion_matrix, cmap = plt.cm.Reds, fignum = 1)
        for i in range(len(total_confusion_matrix)):
            for j in range(len(total_confusion_matrix)):
                plt.annotate(total_confusion_matrix[j,i], xy = (i, j), horizontalalignment='center', verticalalignment='center')
        plt.xlabel('Predicted')
        plt.ylabel('True Label')
        cmName = self.classNames
        index_ls = []
        for i in range(len(cmName)):
            index_ls.append(i)
        plt.xticks(index_ls, cmName)
        plt.yticks(index_ls, cmName)    
        if savepath is not None:
            plt.savefig(savepath)
        plt.show()
        return
    
