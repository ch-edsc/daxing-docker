import os
import cv2
import time
import argparse
import torch
import warnings
import numpy as np
import sys

from detector import build_detector
from deep_sort import build_tracker
from utils.draw import draw_boxes, draw_boxes2
from utils.parser import get_config
from utils.log import get_logger
from utils.io import write_results

#coco 80 類 大車 id 5 ，小車 2 ，機車3
#自行訓練的 大車 id 1 ，小車 2 ，機車3

#BigCarID = 5
#SmallCarID = 2
#MotoBikeID = 3

BigCarID = 1
SmallCarID = 2
MotoBikeID = 3

def show(mat, name='___', scale = 1, pause = False):
    #運算過程的 show 圖放這邊，到時候要關掉直接把下面改 False 就好
    if True:
        if scale != 1:
            h, w = mat.shape
            scaleMat = cv2.resize(mat, (int(w * scale), int(h * scale)))
            cv2.imshow(name, scaleMat)
        else:
            cv2.imshow(name, mat)
        if pause :
            cv2.waitKey(0)  
    return

def exit(code=0):
    # OpenCV 結束前不把它產生的視窗關閉的話會 crash
    cv2.destroyAllWindows()
    sys.exit(code) 



class VideoTracker(object):
    def __init__(self, cfg, args, video_path):
        self.cfg = cfg
        self.args = args
        self.video_path = video_path
        self.logger = get_logger("root")

        use_cuda = args.use_cuda and torch.cuda.is_available()
        if not use_cuda:
            warnings.warn("Running in cpu mode which maybe very slow!", UserWarning)

        if args.display:
            cv2.namedWindow("test", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("test", args.display_width, args.display_height)

        if args.cam != -1:
            print("Using webcam " + str(args.cam))
            self.vdo = cv2.VideoCapture(args.cam)
        else:
            self.vdo = cv2.VideoCapture()
        self.detector = build_detector(cfg, use_cuda=use_cuda)
        self.deepsort1 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort2 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort3 = build_tracker(cfg, use_cuda=use_cuda)
        self.class_names = self.detector.class_names
        
        
        self.isAreaNeedInit = True
        self.inArea1_1 = set()
        self.inArea1_2 = set()
        
        self.inArea2_1 = set()
        self.inArea2_2 = set()
        
        self.inArea3_1 = set()
        self.inArea3_2 = set()
        
        self.inArea4_1 = set()
        self.inArea4_2 = set()
        
        self.outArea1_1 = set()
        self.outArea1_2 = set()
        
        self.outArea2_1 = set()
        self.outArea2_2 = set()
        
        self.outArea3_1 = set()
        self.outArea3_2 = set()
        
        self.outArea4_1 = set()
        self.outArea4_2 = set()
        
        self.turnRightCar = set()
        self.straightCar = set()
        self.turnLeftCar = set()
        
        self.turnRightBigCar = set()
        self.straightBigCar = set()
        self.turnLeftBigCar = set()
        
        self.turnRightMotorbike = set()
        self.straightMotorbike = set()
        self.turnLeftMotorbike = set()
        
        self.max_x = 703
        self.max_y = 575
        
        

    def __enter__(self):
        if self.args.cam != -1:
            ret, frame = self.vdo.read()
            assert ret, "Error: Camera error"
            self.im_width = frame.shape[0]
            self.im_height = frame.shape[1]

        else:
            assert os.path.isfile(self.video_path), "Path error"
            self.vdo.open(self.video_path)
            self.im_width = int(self.vdo.get(cv2.CAP_PROP_FRAME_WIDTH))
            self.im_height = int(self.vdo.get(cv2.CAP_PROP_FRAME_HEIGHT))
            assert self.vdo.isOpened()

        if self.args.save_path:
            os.makedirs(self.args.save_path, exist_ok=True)

            # path of saved video and results
            #self.save_video_path = os.path.join(self.args.save_path, "results.avi")
            self.save_video_path = os.path.join(self.args.save_path, "results_yolov3.mp4")
            self.save_results_path = os.path.join(self.args.save_path, "results.txt")

            # create video writer
            #fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            fourcc = cv2.VideoWriter_fourcc(*'MP4V')
            self.writer = cv2.VideoWriter(self.save_video_path, fourcc, 6.7, (self.im_width, self.im_height))

            # logging
            self.logger.info("Save results to {}".format(self.args.save_path))

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print(exc_type, exc_value, exc_traceback)
        
    def atArea_old(self, rect, point):
        x = point[0]
        y = point[1]
        if x > rect[0][0] and x < rect[1][0] and y > rect[0][1] and y < rect[1][1]:
            return True
        else:
            return False
        
    def atArea(self, arr, point):
        x = point[0]
        y = point[1]
        return arr[y][x]
    
    def matToHotArea(self, mat1, mat2, isIn = True):
        gray1 = cv2.cvtColor(mat1, cv2.COLOR_BGR2GRAY)
        __, gray1 = cv2.threshold(gray1, 10, 255, cv2.THRESH_BINARY)
        gray2 = cv2.cvtColor(mat2, cv2.COLOR_BGR2GRAY)
        __, gray2 = cv2.threshold(gray2, 10, 255, cv2.THRESH_BINARY)

        area1 = np.zeros(gray1.shape, np.bool_)
        area1[gray1 > 10] = 1 
        area2 = np.zeros(gray2.shape, np.bool_)
        area2[gray2 > 10] = 1
        arr =  []
        arr.append(area1.tolist())
        arr.append(area2.tolist())
        
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(2,2))
        grad1 = cv2.morphologyEx(gray1, cv2.MORPH_GRADIENT, kernel)
        grad2 = cv2.morphologyEx(gray2, cv2.MORPH_GRADIENT, kernel)
        
        grad1 = cv2.cvtColor(grad1, cv2.COLOR_GRAY2BGR)
        grad2 = cv2.cvtColor(grad2, cv2.COLOR_GRAY2BGR)

        if isIn:
            self.hotAreasMat = cv2.add(self.hotAreasMat, cv2.multiply(grad1, (0, 0.7, 0, 1)))
            self.hotAreasMat = cv2.add(self.hotAreasMat, cv2.multiply(grad2, (0, 0.5, 0, 1)))
        else:
            self.hotAreasMat = cv2.add(self.hotAreasMat, cv2.multiply(grad1, (0, 0, 0.7, 1)))
            self.hotAreasMat = cv2.add(self.hotAreasMat, cv2.multiply(grad2, (0, 0, 0.5, 1)))
        
        return arr
        
        
    def checkArea(self, img, results):
        
        if self.isAreaNeedInit:            
            self.hotAreasMat = np.zeros(img.shape, np.uint8)
            

            self.areaIn1 = self.matToHotArea(cv2.imread("output/in1_a.png"), cv2.imread("output/in1_b.png"))
            self.areaIn2 = self.matToHotArea(cv2.imread("output/in2_a.png"), cv2.imread("output/in2_b.png"))
            self.areaIn3 = self.matToHotArea(cv2.imread("output/in3_a.png"), cv2.imread("output/in3_b.png"))
            self.areaIn4 = self.matToHotArea(cv2.imread("output/in4_a.png"), cv2.imread("output/in4_b.png"))
            
            self.areaOut1 = self.matToHotArea(cv2.imread("output/out1_a.png"), cv2.imread("output/out1_b.png"), False)
            self.areaOut2 = self.matToHotArea(cv2.imread("output/out2_a.png"), cv2.imread("output/out2_b.png"), False)
            self.areaOut3 = self.matToHotArea(cv2.imread("output/out3_a.png"), cv2.imread("output/out3_b.png"), False)
            self.areaOut4 = self.matToHotArea(cv2.imread("output/out4_a.png"), cv2.imread("output/out4_b.png"), False)
            
            
            #gray1 = cv2.cvtColor(mat1, cv2.COLOR_BGR2GRAY)
            self.isAreaNeedInit = False
            self.trackMap = np.zeros(img.shape, np.uint8)
            self.max_x = img.shape[1] - 1
            self.max_y = img.shape[0] - 1
        

        
        for frame_id, tlwhs, track_ids, class_id in results:
            for tlwh, track_id in zip(tlwhs, track_ids):
                x1, y1, w, h = tlwh
                x = x1 + w//2
                y = y1 + h//2
                if x > self.max_x:
                    x = self.max_x
                elif x < 0:
                    x = 0
                if y > self.max_y:
                    y = self.max_y
                elif y < 0:
                    y = 0
                target = (track_id, class_id)
                self.trackMap[y][x] = [track_id*23%200+55, track_id*47%200+55, track_id*7%200+55]
                
                #右邊進
                if self.atArea(self.areaIn1[0], (x, y)):
                    if target not in self.inArea1_2 \
                    and target not in self.inArea2_1 \
                    and target not in self.inArea3_1 \
                    and target not in self.inArea4_1:
                        self.inArea1_1.add((track_id, class_id))
                if self.atArea(self.areaIn1[1], (x, y)):
                    if(target in self.inArea1_1):
                        if target not in self.inArea1_2:
                            self.inArea1_2.add(target)
                #左邊進
                if self.atArea(self.areaIn2[0], (x, y)):
                    if target not in self.inArea2_2 \
                    and target not in self.inArea1_1 \
                    and target not in self.inArea3_1 \
                    and target not in self.inArea4_1:
                        self.inArea2_1.add((track_id, class_id))
                if self.atArea(self.areaIn2[1], (x, y)):
                    if(target in self.inArea2_1):
                        if target not in self.inArea2_2:
                            self.inArea2_2.add(target)
                #上方進            
                if self.atArea(self.areaIn3[0], (x, y)):
                    if target not in self.inArea3_2\
                    and target not in self.inArea1_1 \
                    and target not in self.inArea2_1 \
                    and target not in self.inArea4_1:
                        self.inArea3_1.add((track_id, class_id))
                if self.atArea(self.areaIn3[1], (x, y)):
                    if(target in self.inArea3_1):
                        if target not in self.inArea3_2:
                            self.inArea3_2.add(target)
                #下方進            
                if self.atArea(self.areaIn4[0], (x, y)):
                    if target not in self.inArea4_2\
                    and target not in self.inArea1_1 \
                    and target not in self.inArea2_1 \
                    and target not in self.inArea3_1:
                        self.inArea4_1.add((track_id, class_id))
                if self.atArea(self.areaIn4[1], (x, y)):
                    if(target in self.inArea4_1):
                        if target not in self.inArea4_2:
                            self.inArea4_2.add(target)
                            

                #上方出
                if self.atArea(self.areaOut1[0], (x, y)):
                    if target not in self.outArea1_2:
                        self.outArea1_1.add((track_id, class_id))
                if self.atArea(self.areaOut1[1], (x, y)):
                    if(target in self.outArea1_1):
                        if target not in self.outArea1_2:
                            self.outArea1_2.add(target)
                            #20210510 上頭指示大車小車機車分開統計
                            #大車 id 5 ，小車 2 ，機車3
                            if target in self.inArea1_2:
                                #右進上出為右轉
                                if  target[1] == SmallCarID:
                                    self.turnRightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnRightMotorbike.add(target)
                                else:
                                    self.turnRightBigCar.add(target)
                            elif target in self.inArea2_2:
                                #左進上出為左轉
                                if  target[1] == SmallCarID:
                                    self.turnLeftCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnLeftMotorbike.add(target)
                                else:
                                    self.turnLeftBigCar.add(target)
                            elif target in self.inArea4_2:
                                #下進上出為直行
                                if  target[1] == SmallCarID:
                                    self.straightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.straightMotorbike.add(target)
                                else:
                                    self.straightBigCar.add(target)
                                
                #左邊出                
                if self.atArea(self.areaOut2[0], (x, y)):
                    if target not in self.outArea2_2:
                        self.outArea2_1.add((track_id, class_id))
                if self.atArea(self.areaOut2[1], (x, y)):
                    if(target in self.outArea2_1):
                        if target not in self.outArea2_2:
                            self.outArea2_2.add(target)
                            if target in self.inArea3_2:
                                if  target[1] == SmallCarID:
                                    self.turnRightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnRightMotorbike.add(target)
                                else:
                                    self.turnRightBigCar.add(target)
                            elif target in self.inArea4_2:
                                if  target[1] == SmallCarID:
                                    self.turnLeftCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnLeftMotorbike.add(target)
                                else:
                                    self.turnLeftBigCar.add(target)
                            elif target in self.inArea1_2:
                                if  target[1] == SmallCarID:
                                    self.straightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.straightMotorbike.add(target)
                                else:
                                    self.straightBigCar.add(target)
                                
                #下方出                
                if self.atArea(self.areaOut3[0], (x, y)):
                    if target not in self.outArea3_2:
                        self.outArea3_1.add((track_id, class_id))
                if self.atArea(self.areaOut3[1], (x, y)):
                    if(target in self.outArea3_1):
                        if target not in self.outArea3_2:
                            self.outArea3_2.add(target)
                            if target in self.inArea2_2:
                                if  target[1] == SmallCarID:
                                    self.turnRightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnRightMotorbike.add(target)
                                else:
                                    self.turnRightBigCar.add(target)
                            elif target in self.inArea1_2:
                                if  target[1] == SmallCarID:
                                    self.turnLeftCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnLeftMotorbike.add(target)
                                else:
                                    self.turnLeftBigCar.add(target)
                            elif target in self.inArea3_2:
                                if  target[1] == SmallCarID:
                                    self.straightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.straightMotorbike.add(target)
                                else:
                                    self.straightBigCar.add(target)
                                
                #右方出                
                if self.atArea(self.areaOut4[0], (x, y)):
                    if target not in self.outArea4_2:
                        self.outArea4_1.add((track_id, class_id))
                if self.atArea(self.areaOut4[1], (x, y)):
                    if(target in self.outArea4_1):
                        if target not in self.outArea4_2:
                            self.outArea4_2.add(target)
                            if target in self.inArea4_2:
                                if  target[1] == SmallCarID:
                                    self.turnRightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnRightMotorbike.add(target)
                                else:
                                    self.turnRightBigCar.add(target)
                            elif target in self.inArea3_2:
                                if  target[1] == SmallCarID:
                                    self.turnLeftCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.turnLeftMotorbike.add(target)
                                else:
                                    self.turnLeftBigCar.add(target)
                            elif target in self.inArea2_2:
                                if  target[1] == SmallCarID:
                                    self.straightCar.add(target)
                                elif target[1] == MotoBikeID:
                                    self.straightMotorbike.add(target)
                                else:
                                    self.straightBigCar.add(target)
        '''
        rect_in1_a = [(550, 186), (704, 204)]
        rect_in1_b = [(470, 212), (704, 234)]
        cv2.rectangle(img, rect_in1_a[0], rect_in1_a[1], [255,0,0], 2)
        cv2.rectangle(img, rect_in1_b[0], rect_in1_b[1], [0,255,0], 2)
        
        
        rect_out1_a = [(133, 150), (232, 172)]
        rect_out1_b = [(41, 146), (119, 171)]
        cv2.rectangle(img, rect_out1_a[0], rect_out1_a[1], [255,0,0], 2)
        cv2.rectangle(img, rect_out1_b[0], rect_out1_b[1], [0,255,0], 2)
        
        
        for frame_id, tlwhs, track_ids, class_id in results:
            for tlwh, track_id in zip(tlwhs, track_ids):
                x1, y1, w, h = tlwh
                x = x1 + w//2
                y = y1 + h//2
                target = (track_id, class_id)
                if self.atArea(rect_in1_a, (x, y)):
                    if target not in self.inArea2:
                        self.inArea1.add((track_id, class_id))
                if self.atArea(rect_in1_b, (x, y)):
                    if(target in self.inArea1):
                        if target not in self.inArea2:
                            print("In: " + str(target[0]) + self.class_names[target[1]])
                            self.inArea2.add(target)
                            
                if self.atArea(rect_out1_a, (x, y)):
                    if target not in self.outArea2:
                        self.outArea1.add((track_id, class_id))
                if self.atArea(rect_out1_b, (x, y)):
                    if(target in self.outArea1):
                        if target not in self.outArea2:
                            print("Out: " + str(target[0]) + self.class_names[target[1]])
                            self.outArea2.add(target)
                            if target in self.inArea2:
                                self.turnRightCar.add(target)
        '''
        return
    def countLossTracks(self):
        totalInSet = self.inArea1_2.union(self.inArea2_2).union(self.inArea3_2).union(self.inArea4_2)
        totalOutSet = self.outArea1_2.union(self.outArea2_2).union(self.outArea3_2).union(self.outArea4_2)
        lossTrackSet = totalInSet.difference(totalOutSet)
        print("lossTrackSet:")
        text_file = open("lossTrackSet.txt", "w")
        text_file.write("===Loss Tracks===\r\n")
        for tar in lossTrackSet:
            print("[%s]%d"  % (self.class_names[tar[1]], tar[0]))
            text_file.write("[%s]%d\r\n"  % (self.class_names[tar[1]], tar[0]))
        
        print("All In Track:")
        text_file.write("===In Tracks===\r\n")
        for tar in totalInSet:
            print("[%s]%d"  % (self.class_names[tar[1]], tar[0]))
            text_file.write("[%s]%d\r\n"  % (self.class_names[tar[1]], tar[0]))
        
        print("All Out Track:")
        text_file.write("===Out Tracks===\r\n")
        for tar in totalInSet:
            print("[%s]%d"  % (self.class_names[tar[1]], tar[0]))
            text_file.write("[%s]%d\r\n"  % (self.class_names[tar[1]], tar[0]))
        text_file.close()
        return

    def run(self):
        results = []
        idx_frame = 0
        while self.vdo.grab():
            idx_frame += 1
            #if idx_frame > 100:
            #    break
            if idx_frame % self.args.frame_interval:
                continue

            start = time.time()
            _, ori_im = self.vdo.retrieve()
            im = cv2.cvtColor(ori_im, cv2.COLOR_BGR2RGB)
            if idx_frame == 1:
                cv2.imwrite("output/temp.jpg", ori_im)
            # do detection
            bbox_xywh, cls_conf, cls_ids = self.detector(im)
            #print("bbox_xywh")
            #print(bbox_xywh)
            #print("cls_conf")
            #print(cls_conf)
            #print("cls_ids")
            #print(cls_ids)

            # select person class
            #mask1 = cls_ids == 2
            #mask2 = cls_ids == 3
            #mask3 = cls_ids == 5
            mask1 = cls_ids == SmallCarID
            mask2 = cls_ids == MotoBikeID
            mask3 = cls_ids == BigCarID
            
            bbox_xywh1 = bbox_xywh[mask1]
            # bbox dilation just in case bbox too small, delete this line if using a better pedestrian detector
            bbox_xywh1[:, 3:] *= 1.2
            bbox_xywh2 = bbox_xywh[mask2]
            bbox_xywh2[:, 3:] *= 1.2
            bbox_xywh3 = bbox_xywh[mask3]
            bbox_xywh3[:, 3:] *= 1.2
            #print(cls_conf)
            #print(cls_ids)
            cls_conf1 = cls_conf[mask1]
            cls_conf2 = cls_conf[mask2]
            cls_conf3 = cls_conf[mask3]


            # do tracking
            outputs1 = self.deepsort1.update(bbox_xywh1, cls_conf1, im)
            outputs2 = self.deepsort2.update(bbox_xywh2, cls_conf2, im)
            outputs3 = self.deepsort3.update(bbox_xywh3, cls_conf3, im)

            im2 = im.copy()
            #im2 = cv2.resize(im2, (512, 512))
            #(576, 704, 3)
            #torch.Size([1, 3, 416, 416])
            '''
            for box in bbox_xywh:
                
                #print(box)
                #box*=0.95
                x1 = int(box[0])
                y1 = int(box[1])
                x2 = int(box[0] + box[2])
                y2 = int(box[1] + box[3])
                #x1 -= 40
                #x2 -= 40
                #y1 -= 20
                #y2 -= 20
                #x1 *= 416/576.0
                #x2 *= 416/704.0
                #y1 *= 416/576.0
                #y2 *= 416/704.0
                x1 = int(x1)
                x2 = int(x2)
                y1 = int(y1)
                y2 = int(y2)
                
                #x1 += 100
                print("=====")
                print(x1)
                print(x2)
                print(y1)
                print(y2)
                cv2.rectangle(im2, (x1, y1), (x2, y2), (0, 0, 255), 1, cv2.LINE_AA)
            cv2.imshow("temp", im2)
            '''
            #cv2.waitKey(0)
            
            # draw boxes for visualization
            outputs_one_frame = []
            outputs = outputs1
            classID = 2
            if len(outputs) > 0:
                bbox_tlwh = []
                bbox_xyxy1 = outputs[:, :4]
                identities = outputs[:, -1]
                ori_im = draw_boxes2(ori_im, bbox_xyxy1, identities, self.class_names[classID])

                for bb_xyxy in bbox_xyxy1:
                    bbox_tlwh.append(self.deepsort1._xyxy_to_tlwh(bb_xyxy))

                results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))


            outputs = outputs2
            classID = 3
            if len(outputs) > 0:
                bbox_tlwh = []
                bbox_xyxy2 = outputs[:, :4]
                identities = outputs[:, -1]
                ori_im = draw_boxes2(ori_im, bbox_xyxy2, identities, self.class_names[classID])

                for bb_xyxy in bbox_xyxy2:
                    bbox_tlwh.append(self.deepsort2._xyxy_to_tlwh(bb_xyxy))

                results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                
            outputs = outputs3
            classID = 5
            if len(outputs) > 0:
                bbox_tlwh = []
                bbox_xyxy3 = outputs[:, :4]
                identities = outputs[:, -1]
                ori_im = draw_boxes2(ori_im, bbox_xyxy3, identities, self.class_names[classID])

                for bb_xyxy in bbox_xyxy3:
                    bbox_tlwh.append(self.deepsort3._xyxy_to_tlwh(bb_xyxy))

                results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                
                
                
            self.checkArea(ori_im, outputs_one_frame)
            
            #show(self.trackMap, "tracks")
            #show(ori_im, "vedios")
            #cv2.waitKey(15)
            
            #畫熱區螢幕太亂...不畫了
            #ori_im = cv2.add(self.hotAreasMat, ori_im)
            
            
            textColor = [0, 0, 0]
            textColorB = [0, 255, 255]
            
            px = 10
            py = 20
            text = "Small Car:"
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "turnRight: %d"  % len(self.turnRightCar)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "straight: %d"  % len(self.straightCar)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "turnLeft: %d"  % len(self.turnLeftCar)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            
            px = 160
            py = 20
            text = "Big Car:"
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "turnRight: %d"  % len(self.turnRightBigCar)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "straight: %d"  % len(self.straightBigCar)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "turnLeft: %d"  % len(self.turnLeftBigCar)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            
            px = 310
            py = 20
            text = "Motorbike:"
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "turnRight: %d"  % len(self.turnRightMotorbike)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "straight: %d"  % len(self.straightMotorbike)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "turnLeft: %d"  % len(self.turnLeftMotorbike)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            
            
            textColorB = [0, 255, 0]
            px = 500
            py = 20
            text = "Right In: %d"  % len(self.inArea1_2)
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "Left In: %d"  % len(self.inArea2_2)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "Up In: %d"  % len(self.inArea3_2)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "Down In: %d"  % len(self.inArea4_2)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            
            
            px = 500
            py = 120
            text = "Right Out: %d"  % len(self.outArea4_2)
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "Left Out: %d"  % len(self.outArea2_2)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+20), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "Up Out: %d"  % len(self.outArea1_2)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+40), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            text = "Down Out: %d"  % len(self.outArea3_2)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColor, 5)
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, 1.2, textColorB, 2)
            
            #print(self.hotAreasMat.shape)
            #print(ori_im.hotAreasMat.shape)
            

            end = time.time()
            
            #cv2.imshow("test", ori_im)
            #cv2.waitKey(0)

            if self.args.display:
                cv2.imshow("test", ori_im)
                cv2.waitKey(1)

            if self.args.save_path:
                self.writer.write(ori_im)
                

            # save results
            write_results(self.save_results_path, results, 'mot')
            

            # logging
            self.logger.info("time: {:.03f}s, fps: {:.03f}, detection numbers: {}, tracking numbers: {}" \
                             .format(end - start, 1 / (end - start), bbox_xywh.shape[0], len(outputs_one_frame)))
        
        #for target in self.turnRightCar:
        #    print(target)
        self.countLossTracks()
        cv2.destroyAllWindows()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("VIDEO_PATH", type=str)
    parser.add_argument("--config_detection", type=str, default="./configs/yolov3.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    # parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
    parser.add_argument("--display", action="store_true")
    parser.add_argument("--frame_interval", type=int, default=1)
    parser.add_argument("--display_width", type=int, default=800)
    parser.add_argument("--display_height", type=int, default=600)
    parser.add_argument("--save_path", type=str, default="./output/")
    parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
    parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    cfg = get_config()
    cfg.merge_from_file(args.config_detection)
    cfg.merge_from_file(args.config_deepsort)

    with VideoTracker(cfg, args, video_path=args.VIDEO_PATH) as vdo_trk:
        vdo_trk.run()
