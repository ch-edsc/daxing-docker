# -*- coding: utf-8 -*-
import os
import cv2
import argparse
import numpy as np
import sys

cameraNo = "14"

# cd C:\Users\leo_1\Documents\GitHub\daxing-code\deep_sort_pytorch
# python test_draw_turn.py 
class Car:
    def __init__(self, carID = 0, carType = 2):
        self.carType = carType #車種
        self.carID = carID #車種
        self.tracks = [] #軌跡紀錄，目前為[x, y, w, h, frameID]
        #以後根據情況看要不要擴充車子長寬/出現時間點等等數值進去
        self.clusterType = 0
        self.lastFrame = 0#紀錄最後一次更新時的frame
        return
    '''        
    def add(self, x, y):
        #幾乎沒移動時不計算
        if len(self.tracks) > 0:
            last = self.tracks[len(self.tracks) - 1]
            d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
            if d > 400:
                self.tracks.append(np.array([x, y]))
        else:
            self.tracks.append(np.array([x, y]))
        return
    '''
    
    def add(self, x, y, w, h, frameID):
        self.tracks.append(np.array([x, y, w, h, frameID]))
        #if len(self.tracks) > 0:
        #    last = self.tracks[len(self.tracks) - 1]
        #    d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
        #    if d > 400:
        #        self.tracks.append(np.array([x, y, w, h, frameID]))
        #else:
        #    self.tracks.append(np.array([x, y, w, h, frameID]))
        return

    def getAngle(self, v1, v2):
        #向量求夾角，來源 https://www.it145.com/9/94136.html
        x1 = v1[0]
        y1 = v1[1]
        x2 = v2[0]
        y2 = v2[1]
        theta = np.arctan2(x1 * y2 - y1 * x2, x1 * x2 + y1 * y2)
        return theta * 180 / np.pi

def drawCells(img, cells):
    imw = img.shape[1]
    imh = img.shape[0]
    for i in range(cells):
        tarw = int(i * (imw/cells))
        tarh = int(i * (imh/cells))
        cv2.line(img, (0, tarh), (imw, tarh), (255, 255, 255), 1)
        cv2.line(img, (tarw, 0), (tarw, imh), (255, 255, 255), 1)
    return img

videoPath = "data/camera_" + cameraNo + "_2021-09-20-09-00-00.mp4"
clusteringDataPath = "data/tracks" + cameraNo + ".csv"



npfile = np.loadtxt(clusteringDataPath,dtype=np.int,delimiter=',')
print("read data OK")
carList = {}
for i in range(len(npfile)):
    line = npfile[i]
    car = Car(line[0], line[1])
    #car.clusterType = line[0]
    for j in range(2, len(line), 5):
        if line[j+4] == 0:
            break
        car.add(line[j], line[j+1], line[j+2], line[j+3], line[j+4])
    carList[(car.carID, car.carType)] = car

print("set car track OK")



vdo = cv2.VideoCapture()
vdo.open(videoPath)
im_width = int(vdo.get(cv2.CAP_PROP_FRAME_WIDTH))
im_height = int(vdo.get(cv2.CAP_PROP_FRAME_HEIGHT))
#print(im_width)
#print(im_height)
#exit()


num = 1
for key, car in carList.items():
    img = np.zeros((im_height, im_width, 3), np.uint8)
    img2 = cv2.imread("output/temp%s.png" % cameraNo)
    for i in range(len(car.tracks)):
        track = car.tracks[i]
        xc = track[0] + track[2] // 2
        yc = track[1] + track[3] // 2
        
        if i > 0:
            preTrack = car.tracks[i-1]
            pxc = preTrack[0] + preTrack[2] // 2
            pyc = preTrack[1] + preTrack[3] // 2
            if i < len(car.tracks) // 5:
                cv2.line(img, (xc, yc), (pxc, pyc), (255, 255, 0), 9)
                cv2.line(img2, (xc, yc), (pxc, pyc), (255, 255, 0), 9)
            else:
                cv2.line(img, (xc, yc), (pxc, pyc), (255, 0, 0), 5)
                cv2.line(img2, (xc, yc), (pxc, pyc), (255, 0, 0), 5)
        
    for i in range(len(car.tracks)):
        track = car.tracks[i]
        xc = track[0] + track[2] // 2
        yc = track[1] + track[3] // 2
        cv2.circle(img, (xc, yc), 2, (0, 0, 255), -1)
        cv2.circle(img2, (xc, yc), 2, (0, 0, 255), -1)
    cv2.putText(img2, "%d, %d" % (car.carID, car.carType) , (25, 25), cv2.FONT_HERSHEY_PLAIN, 2, (100, 0, 100), 2)
    cv2.imwrite("data/trackMap/%d_t.png" % num, img2)
    cv2.imwrite("data/trackMap/%d.png" % num, img)
    num += 1

exit(0)
