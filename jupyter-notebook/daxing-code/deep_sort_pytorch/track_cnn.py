# -*- coding: utf-8 -*-


import numpy as np
import tensorflow as tf
import os

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, Flatten, Dropout, BatchNormalization, AveragePooling2D, Input
from tensorflow.keras.preprocessing import image


from utils.confusion_matrix import CM


import cv2

'''
Epoch 100/100
19/19 [==============================] - 3s 166ms/step - loss: 0.1288 - accuracy: 0.9983 - val_loss: 0.2497 - val_accuracy: 0.9870
'''

def predict(imagePath, modelPath):
    #預測，參照 https://www.tensorflow.org/guide/keras/save_and_serialize
    model = tf.keras.models.load_model(modelPath)
    fileList = os.listdir(imagePath)
    #testDir2 = os.listdir("data/test/outdoor/")
    result = []
    for path in fileList:
        path = imagePath + path
        print(path)
        #img = image.load_img(path, target_size=(100, 100))
        #x = image.img_to_array(img)
        x = cv2.imread(path)
        x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
        x = cv2.resize(x, (100, 100))
        x = x / 255.0
        
        x = np.expand_dims(x, axis=0)
        y = model.predict_classes(x)
        print(y[0])
        result.append(y[0])
    return result
            
#這邊切換要訓練還是要預測
#mode = 'train'
mode = 'predict'

if __name__ == '__main__':

    if mode == 'train':
        

        train_datagen = ImageDataGenerator(
        rescale=1./255,
        channel_shift_range=20)
        
        test_datagen = ImageDataGenerator(rescale=1./255)
        
        train_generator = train_datagen.flow_from_directory(
            'data/tracks/train',
            target_size=(100, 100),
            batch_size=32,
            shuffle=True,
            class_mode='categorical')
        validation_generator = test_datagen.flow_from_directory(
            'data/tracks/val',
            target_size=(100, 100),
            batch_size=32,
            class_mode='categorical')
        

        
        
        # 應用 SqueezeNet 概念，加入 1x1 卷積
        # 這組參數 loss: 0.1809 - accuracy: 0.9291 - val_loss: 0.0530 - val_accuracy: 1.0000
        # model 0.5 MB 差不多夠小了，就用這組了
        n = 20
        lr = 0.00005
        epoch = 100
        model = Sequential()
        model.add(Conv2D(n*2, 5, activation='relu', padding='same', input_shape=[100, 100, 3]))
        model.add(Conv2D(n, 1, activation='relu', padding='same'))
        model.add(MaxPool2D(pool_size=(2,2)))
        model.add(BatchNormalization())
        
        model.add(Conv2D(n*2, 5, activation='relu', padding='same'))
        model.add(Conv2D(n, 1, activation='relu', padding='same'))
        model.add(MaxPool2D(pool_size=(2,2)))
        model.add(BatchNormalization())
        #model.add(Dropout(0.5))
        
        model.add(Conv2D(n, 3, activation='relu', padding='same'))
        model.add(Conv2D(n, 1, activation='relu', padding='same'))
        model.add(AveragePooling2D(pool_size=(5,5))) #縮5x5用MaxPool砍掉太多資訊，改用AveragePool
        model.add(BatchNormalization())
        #model.add(Dropout(0.5))
        
        model.add(Conv2D(n, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*2, 1, activation='relu', padding='same'))
        model.add(AveragePooling2D(pool_size=(5,5)))
        #model.add(Dropout(0.5))
        #model.add(BatchNormalization())
        
        model.add(Flatten())
        #model.add(Dropout(0.5))
        model.add(BatchNormalization()) #查了一些資料，有人說 BN 與 DropOut 不要一起用，見 https://arxiv.org/abs/1801.05134
        model.add(Dense(21, activation='softmax'))
        
        model.summary()   
        
        opt = tf.keras.optimizers.Adam(learning_rate=lr)
        model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    
        #開始 train
        model.fit(
            train_generator,
            epochs=epoch,
            validation_data=validation_generator)
        model.save('model/model.h5')
        
        #然後預測
        predictModelPath = "model/model.h5"
        predict("data/tracks/val/00/", predictModelPath)
        predict("data/tracks/val/01/", predictModelPath)
    elif mode == 'predict':
        #這邊是不train，直接讀入model預測
        predictModelPath = "model/model.h5"
        #類別順序 [右直、右左、右右、右兩段、右迴轉、左直、左左.....上直、....下直...其他]
        myNames = ["RS", "RL", "RR", "RTwo", "RBack",
                   "LS", "LL", "LR", "LTwo", "LBack",
                   "US", "UL", "UR", "UTwo", "UBack",
                   "DS", "DL", "DR", "DTwo", "DBack",
                   "None"]

        cm = CM(21, myNames)
        
        for i in range(21):
            result = predict("data/tracks/val/%02d/" % (i), predictModelPath)
            for j in result:
                cm.addOne(j, i)
        cm.draw()