import os
import cv2
import time
import argparse
import torch
import warnings
import numpy as np
import sys
import json
import copy

from detector import build_yolor_detector
from deep_sort import build_tracker
from utils.draw import draw_boxes, draw_boxes2, compute_color_for_labels
from utils.parser import get_config
from utils.log import get_logger
from utils.io import write_results

import sqlite3
from scipy.interpolate import interp1d

import tensorflow as tf

#20211123輸出excel
from openpyxl import load_workbook

excel_format_file_path = "data/output_format.xlsx"



# cd C:\Users\leo_1\Documents\GitHub\daxing-code\deep_sort_pytorch
# python yolor_deepsort_new.py data/camera_3_2021-09-20-09-00-00.mp4
# python yolor_deepsort_new.py data/camera_14_2021-09-20-09-00-00.mp4
#coco 80 類 大車 id 5 ，小車 2 ，機車3
#自行訓練的 大車 id 1 ，小車 2 ，機車3，行人4

#BigCarID = 5
#SmallCarID = 2
#MotoBikeID = 3

BigCarID = 1
SmallCarID = 2
MotoBikeID = 3
PeopleID = 4



'''
20210814
接下來主要任務有
1. 對deepsort tracking方式改良
2. 對轉彎車/直行車判斷改良
3. 兩段式左轉的判斷

這週先做[對轉彎車/直行車判斷改良]
結構設計：
定義物件 Car，紀錄車種/出現在每個frame的座標點
然後寫個method用座標點判定車子的行為

20210821
依上周開會討論，要把轉彎車/直行車判斷改成非監督式學習(Kmean/AgglomerativeClustering之類的)
首先需要有把整批data記錄下來的機制/讀回的機制
然後要把各筆軌跡資料的長度轉成相同的機制
然後用sklearn跑Clustering
'''


#output用的json
jsonData = { }

#trunGate = 10
roadPointDataPath = "" #"data/road_camera3.csv"
roadEndPointDataPath = ""

#trunGateDataPath = ""#"data/turn_camera3.csv" #"data/turn_setting_camera_3.txt"
saveTrackDataPath = ""#"data/tracks3.csv"
saveTrackDataWithLabelPath = ""#"data/tracks3_with_label.csv"
videoStartTime_ori = ""

cameraNo = 0 # 預設值0，運行時會從讀入影片的檔名抓
#trunGate = [] # 運行時會從 trunGateDataPath 抓
def readSetting(args):
    #global trunGate
    global cameraNo
    global roadPointDataPath
    global roadEndPointDataPath
    global saveTrackDataPath
    global saveTrackDataWithLabelPath
    global videoStartTime_ori
    videoPath = args.VIDEO_PATH
    temp = videoPath.split("_")
    cameraNo = int(temp[-2])
    videoStartTime_ori = temp[-1].split(".")[0]
    videoStartTime = list(videoStartTime_ori)
    videoStartTime[10] = " "
    videoStartTime[13] = ":"
    videoStartTime[16] = ":"
    videoStartTime = "".join(videoStartTime)

    jsonData["cameraNo"] = cameraNo
    jsonData["videoStartTime"] = videoStartTime
    type2Nums = [3, 14]
    
    if cameraNo in type2Nums:
        jsonData["type"] = 2
    else:
        jsonData["type"] = 1
        
    roadPointDataPath = "data/road_camera" + str(cameraNo) + ".csv"
    roadEndPointDataPath = "data/road_end_camera" + str(cameraNo) + ".csv"
    saveTrackDataPath = "data/tracks" + str(cameraNo) + ".csv"
    saveTrackDataWithLabelPath = "data/tracks" + str(cameraNo) + "_with_label.csv"
    
    
    #trunGateDataPath = "data/turn_setting_camera_" + str(cameraNo) + ".txt"
    
    #file = open(trunGateDataPath, 'r')
    #trunGateStr = file.readline()
    #trunGate = float(trunGateStr)
    #file.close()
    #現在不用trunGate判斷轉向，改用起點與終點
    #trunGateDataPath = "data/turn_camera" + str(cameraNo) + ".txt"
    #trunGate = np.loadtxt(trunGateDataPath, dtype=np.int,delimiter=',')
    
    
    return


def extrapolate(points, minX=0, minY=0, maxX=704, maxY=576):
    #目標是外插到畫面邊緣，也就是 x = 0或704 時 y 在 0~576之間
    #或者 y = 0或576時 x在0~704之間
    xarr = []
    yarr = []
    for i in range(len(points)):
        xarr.append(points[i][0])
        yarr.append(points[i][1])

    if xarr[0] == xarr[1] and yarr[0] == yarr[1]:
        return [xarr[0], yarr[0]]
        
    fy = interp1d(xarr, yarr, fill_value='extrapolate')
    fx = interp1d(yarr, xarr, fill_value='extrapolate')
    tarX = minX
    tarY = minY
    if xarr[0] < xarr[-1]:
        #起始點x比較小，往左邊外插
        tarX = minX
        tarY = fy(tarX).tolist()
    else: 
        #往右邊外插
        tarX = maxX
        tarY = fy(tarX).tolist()
    if (tarY > maxY) or (tarY < 0):
        # y出界了，反過來外插x
        if yarr[0] < yarr[-1]:
            #起始點y比較小，往上方外插
            tarY = minY
            tarX = fx(tarY).tolist()
        else:
            tarY = maxY
            tarX = fx(tarY).tolist()
    
    tarX = max(minX, tarX)
    tarX = min(maxX, tarX)
    tarY = max(minY, tarY)
    tarY = min(maxY, tarY)
    return [tarX, tarY]

class Car:
    def __init__(self, carID = 0, carType = 2):
        self.carType = carType #車種
        self.carID = carID #車種
        self.tracks = [] #軌跡紀錄，目前為[x, y, w, h, frameID]
        #以後根據情況看要不要擴充車子長寬/出現時間點等等數值進去
        self.clusterType = 0
        self.lastFrame = 0#紀錄最後一次更新時的frame
        self.road_points = np.loadtxt(roadPointDataPath, dtype=np.float64,delimiter=',')
        self.road_end_points = np.loadtxt(roadEndPointDataPath, dtype=np.float64,delimiter=',')
        #20211004增加，停等時間計算用
        self.maxWaitSec = 0 #停等秒數
        self.turnID = -1 # 轉向
        self.roadID = -1 # 進入道路
        self.isLeave = False
        self.isCounted = False
        return
    
    def getWaitTimes(self, fps):
        #取得停等時間，預計在車子離開畫面時(也就是取得該車輛得完整軌跡後)呼叫
        
        #基本上畫面中只有一個路口，所以一台車停等時間只會有一次
        #定義為：每台車的[最長連續N個frames沒有移動]除以[FPS]為停等時間
        isWaiting = False
        
        startWaitFrame = 0
        for i in range(len(self.tracks)):
            if i==0:
                continue
            preTrack = self.tracks[i-1]
            nowTrack = self.tracks[i]
            preXc = preTrack[0] + preTrack[2] / 2
            preYc = preTrack[1] + preTrack[3] / 2
            nowXc = nowTrack[0] + nowTrack[2] / 2
            nowYc = nowTrack[1] + nowTrack[3] / 2
            diffX = preXc - nowXc
            diffY = preYc - nowYc
            if diffX * diffX + diffY * diffY < 100: #允許偵測框偏移值10個像素
                #這邊表示車子沒在動
                if isWaiting == False:
                    #車子沒在動，且之前狀態不是等待中，紀錄目標frame (車子開始等待的時間點)
                    isWaiting = True
                    startWaitFrame = nowTrack[4]
            else:
                #這邊表示車子在動
                if isWaiting:
                    #車子在動，且之前狀態為等待中，更新 maxWaitFrames
                    isWaiting = False
                    endWaitFrame = nowTrack[4]
                    waitSec = (endWaitFrame - startWaitFrame) / (float)(fps)
                    if waitSec > self.maxWaitSec:
                        self.maxWaitSec = waitSec
                    
        return
    
    '''        
    def add(self, x, y):
        #幾乎沒移動時不計算
        if len(self.tracks) > 0:
            last = self.tracks[len(self.tracks) - 1]
            d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
            if d > 400:
                self.tracks.append(np.array([x, y]))
        else:
            self.tracks.append(np.array([x, y]))
        return
    '''
    
    def add(self, x, y, w, h, frameID):
        self.tracks.append(np.array([x, y, w, h, frameID]))
        #if len(self.tracks) > 0:
        #    last = self.tracks[len(self.tracks) - 1]
        #    d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
        #    if d > 400:
        #        self.tracks.append(np.array([x, y, w, h, frameID]))
        #else:
        #    self.tracks.append(np.array([x, y, w, h, frameID]))
        return

    def getAngle(self, v1, v2):
        #向量求夾角，來源 https://www.it145.com/9/94136.html
        x1 = v1[0]
        y1 = v1[1]
        x2 = v2[0]
        y2 = v2[1]
        theta = np.arctan2(x1 * y2 - y1 * x2, x1 * x2 + y1 * y2)
        return theta * 180 / np.pi
    
    def predict(self):
        result = -1
        result2 = -1
        getLength = 5
        
        if len(self.tracks) > 10:
            size = len(self.tracks)
            
            
            #計算前 n 點向量
            vector1 = self.tracks[getLength] - self.tracks[0]
            #計算後 n 點向量
            vector2 = self.tracks[size - 1] - self.tracks[size - 1 - getLength]
            angle = self.getAngle(vector1, vector2)
            #print(angle)
            
            #路口計算，目前先寫死，以後看看有沒有辦法自動化
            #對應camera3，由下往上
            #vectorRoad = np.array([-5, -1])
            #對應camera14
            #vectorRoad = np.array([13, -35])
            #觀察進入向量與第一個路口向量的夾角
            '''
            inAngle = self.getAngle(vector1, vectorRoad)
            #print(inAngle)
            if abs(inAngle) < roadGate: #路口1，正向
                result2 = 0 #"road1_A" 
            elif abs(inAngle) > 150: #路口1，反向
                result2 = 1 #"road1_B"
            elif inAngle >= roadGate:
                result2 = 2 #"road2_A"
            else:
                result2 = 3 #"road2_B"
            '''
            
            
            #20211110增加：從頭到尾停住的車不判定
            startPoint = np.array([self.tracks[0][0] + self.tracks[0][2]//2, self.tracks[0][1]  + self.tracks[0][3]//2 ])
            endPoint = np.array([self.tracks[-1][0] + self.tracks[-1][2]//2, self.tracks[-1][1]  + self.tracks[-1][3]//2 ])
            st_ed_diff = np.linalg.norm(startPoint - endPoint)
            if st_ed_diff < 50:
                return -1, -1
            
            #20210915 路口判定改為用分群結果算出來的座標點，看距離哪個路口點最近
            result2 = 0
            
            minDiff = np.linalg.norm(startPoint - self.road_points[0])
            
            for i in range(len(self.road_points)):
                tarDiff = np.linalg.norm(startPoint - self.road_points[i])
                if tarDiff < minDiff:
                    minDiff = tarDiff
                    result2 = i
            
            #20211103轉向改為抓起點/終點路口判斷
            endRoad = 0
            
            minDiff = np.linalg.norm(endPoint - self.road_end_points[0])
            
            for i in range(len(self.road_end_points)):
                tarDiff = np.linalg.norm(endPoint - self.road_end_points[i])
                if tarDiff < minDiff:
                    minDiff = tarDiff
                    endRoad = i
            
            #路口順序 右左上下
            if result2 == 0: #右入
                if endRoad == 0:
                    #右入 右出，迴轉
                    result = 4
                elif endRoad == 1:
                    #右入 左出，直行
                    result = 0
                elif endRoad == 2:
                    #右入 上出，右轉
                    result = 2
                else:
                    #右入 下出，左轉
                    result = 1
            elif result2 == 1: #左入
                if endRoad == 0:
                    #左入 右出，直行
                    result = 0
                elif endRoad == 1:
                    #左入 左出，迴轉
                    result = 4
                elif endRoad == 2:
                    #左入 上出，左轉
                    result = 1
                else:
                    #左入 下出，右轉
                    result = 2      
            elif result2 == 2: #上入
                if endRoad == 0:
                    #上入 右出，左轉
                    result = 1
                elif endRoad == 1:
                    #上入 左出，右轉
                    result = 2
                elif endRoad == 2:
                    #上入 上出，迴轉
                    result = 4
                else:
                    #上入 下出，直行
                    result = 0
            elif result2 == 3: #下入
                if endRoad == 0:
                    #下入 右出，右轉
                    result = 2
                elif endRoad == 1:
                    #下入 左出，左轉
                    result = 1
                elif endRoad == 2:
                    #下入 上出，直行
                    result = 0
                else:
                    #下入 下出，迴轉
                    result = 4
                    
            #兩段式左轉，額外判斷
            #定義為：移動途中發生[右轉超過10度再左轉]定義為兩段式左轉
            #有時候判斷會異常，改為指針對摩托車判斷
            if self.carType == MotoBikeID and result == 1:#如果左轉
                maxRightTurn = 0
                
                for i in range(10, size, 5): #每個點都判斷太花時間，間隔5判斷1次
                    vector2 = self.tracks[i - 1] - self.tracks[i - 1 - getLength]
                    angle = self.getAngle(vector1, vector2)
                    if maxRightTurn < angle:
                        maxRightTurn = angle
                if maxRightTurn > 10:
                    result = 3 #符合條件，把左轉改成兩段式左轉
            
            '''
            #20211027 現在轉向量閾值，4個路口有不同值
            if abs(angle) > 150: #除了左右轉，還有迴轉車
                result = 3 #"trunBack"
            elif angle < -trunGate[result2]:
                result = 1 #"trunLeft"
            elif angle > trunGate[result2]:
                result = 2 #"trunRight"
            else:
                result = 0 #"straight"
            '''  
                
            '''
            print("time: %d " % (self.lastFrame // 7))
            print("ID: %d Type: %d " % (self.carID, self.carType))
            print(vector1)
            print(vector2)
            #print(vectorRoad)
            print(angle)
            print(inAngle)
            print('=======')
            if self.carID == 6:
                for i in self.tracks:
                    print(i)
            '''
        self.turnID = result
        self.roadID = result2
        return result, result2
    
    def trackToMat(self, im_width, im_height):
        #把路徑畫成mat
        img = np.zeros((im_height, im_width, 3), np.uint8)
        for i in range(len(self.tracks)):
            track = self.tracks[i]
            xc = track[0] + track[2] // 2
            yc = track[1] + track[3] // 2
            
            if i > 0:
                preTrack = self.tracks[i-1]
                pxc = preTrack[0] + preTrack[2] // 2
                pyc = preTrack[1] + preTrack[3] // 2
                if i < len(self.tracks) // 5:
                    cv2.line(img, (xc, yc), (pxc, pyc), (255, 255, 0), 9)
                else:
                    cv2.line(img, (xc, yc), (pxc, pyc), (255, 0, 0), 5)
            
        for i in range(len(self.tracks)):
            track = self.tracks[i]
            xc = track[0] + track[2] // 2
            yc = track[1] + track[3] // 2
            cv2.circle(img, (xc, yc), 2, (0, 0, 255), -1)
        
        return img
        
    
    def predictWithModel(self, cnnModel, im_width, im_height):
        #20211012 改用cnn算路口轉彎
        
        #把路徑畫成mat
        img = self.trackToMat(im_width, im_height)
        
        #resize 100x100 轉RGB 然後餵給模型
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (100, 100))
        img = img / 255.0
        
        x = np.expand_dims(img, axis=0)
        y = cnnModel.predict_classes(x)[0]
        
        #類別順序 [右直、右左、右右、右兩段、右迴轉、左直、左左.....上直、....下直...其他] 共21類
        #也就是，y // 5 得到路口， y % 5 得到轉向        
        roadType = y // 5
        turnType = y % 5
                
        result = -1
        result2 = -1
        
        if roadType < 4:
            result = turnType
            result2 = roadType

        self.turnID = result
        self.roadID = result2
        self.clusterType = y
        return result, result2
    




def show(mat, name='___', scale = 1, pause = False):
    #運算過程的 show 圖放這邊，到時候要關掉直接把下面改 False 就好
    if True:
        if scale != 1:
            h = mat.shape[0]
            w = mat.shape[1]
            scaleMat = cv2.resize(mat, (int(w * scale), int(h * scale)))
            cv2.imshow(name, scaleMat)
        else:
            cv2.imshow(name, mat)
        if pause :
            cv2.waitKey(0)  
    return

def exit(code=0):
    # OpenCV 結束前不把它產生的視窗關閉的話會 crash
    cv2.destroyAllWindows()
    sys.exit(code) 



class VideoTracker(object):
    def __init__(self, cfg, args, video_path):
        self.cfg = cfg
        self.args = args
        self.video_path = video_path
        self.logger = get_logger("root")

        use_cuda = args.use_cuda and torch.cuda.is_available()
        if not use_cuda:
            warnings.warn("Running in cpu mode which maybe very slow!", UserWarning)

        if args.display:
            cv2.namedWindow("test", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("test", args.display_width, args.display_height)

        if args.cam != -1:
            print("Using webcam " + str(args.cam))
            self.vdo = cv2.VideoCapture(args.cam)
        else:
            self.vdo = cv2.VideoCapture()
        

        
        # python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --conf-thres 0.5 --img-size 640 --device 0 --names data/custom/car.names --source data/camera3/obj_train_data --output inference/output_camera3
        # opt.output, opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size, opt.cfg, opt.names
        opt = {}
        opt["output"] = "inference/output"
        opt["source"] = "data/obj_train_data/"
        opt["weights"] = "detector/YOLOR/weight/car.pt"
        opt["view_img"] = False
        opt["save_txt"] = False
        opt["img_size"] = 640
        opt["cfg"] = "detector/YOLOR/cfg/yolor_p6.cfg"
        opt["names"] = "detector/YOLOR/cfg/car.names"
        if use_cuda:
          opt["device"] = "0"
        else:
          opt["device"] = "cpu"
        opt["augment"] = False
        opt["conf_thres"] = 0.4
        opt["iou_thres"] = 0.5
        opt["agnostic_nms"] = False
        self.detector = build_yolor_detector(opt, use_cuda=use_cuda)
        self.deepsort1 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort2 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort3 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort4 = build_tracker(cfg, use_cuda=use_cuda)
        self.class_names = self.detector.class_names
        
        
        self.isAreaNeedInit = True
        self.inArea1_1 = set()
        self.inArea1_2 = set()
        
        self.inArea2_1 = set()
        self.inArea2_2 = set()
        
        self.inArea3_1 = set()
        self.inArea3_2 = set()
        
        self.inArea4_1 = set()
        self.inArea4_2 = set()
        
        self.outArea1_1 = set()
        self.outArea1_2 = set()
        
        self.outArea2_1 = set()
        self.outArea2_2 = set()
        
        self.outArea3_1 = set()
        self.outArea3_2 = set()
        
        self.outArea4_1 = set()
        self.outArea4_2 = set()
        
        
        self.turnRightCar = set()
        self.straightCar = set()
        self.turnLeftCar = set()
        
        self.turnRightBigCar = set()
        self.straightBigCar = set()
        self.turnLeftBigCar = set()
        
        self.turnRightMotorbike = set()
        self.straightMotorbike = set()
        self.turnLeftMotorbike = set()
        
        self.carList = {}
        self.lossCarList = {}
        self.occurCarList = {}
        self.matchList = {}
        '''
        20211012轉向定義增加：迴轉與兩段式
        改為各路段分別統計，雙向道，十字路口共4路段，大車小車機車共3種車，然後左右轉/直行共3種狀況，要36個set
        資料用多層結構來放，roads[哪條道路][哪種轉向][哪種車]
        道路順序，先橫向後直向，定義
        roads[0] = 右往左
        roads[1] = 左往右
        roads[2] = 上往下
        roads[3] = 下往上
        轉向定義為
        roads[n][0] = 直行
        roads[n][1] = 左轉
        roads[n][2] = 右轉
        roads[n][3] = 兩段式左轉
        roads[n][4] = 迴轉
        車種定義為
        roads[n][m][0] = 小車
        roads[n][m][1] = 機車
        roads[n][m][2] = 大車
        '''
        self.roads = resetRoadCounting()

        #20211015要改成每分鐘輸出，建一個temproads用來儲存該分鐘統計量
        self.temproads = resetRoadCounting()

                    
        self.infoSpaceWidth = 400
        
        
        self.max_x = 703
        self.max_y = 575
        
        #200211012 改用CNN模型算轉彎與路口
        self.turnModel = tf.keras.models.load_model("model/model.h5")

        
        
        
        

    def __enter__(self):
        if self.args.cam != -1:
            ret, frame = self.vdo.read()
            assert ret, "Error: Camera error"
            self.im_width = frame.shape[0]
            self.im_height = frame.shape[1]

        else:
            assert os.path.isfile(self.video_path), "Path error"
            self.vdo.open(self.video_path)
            self.im_width = int(self.vdo.get(cv2.CAP_PROP_FRAME_WIDTH))
            self.im_height = int(self.vdo.get(cv2.CAP_PROP_FRAME_HEIGHT))
            assert self.vdo.isOpened()

        if self.args.save_path:
            os.makedirs(self.args.save_path, exist_ok=True)

            # path of saved video and results
            #self.save_video_path = os.path.join(self.args.save_path, "results.avi")
            self.save_video_path = os.path.join(self.args.save_path, "results_camera" + str(cameraNo) + ".mp4")
            self.save_results_path = os.path.join(self.args.save_path, "results_camera" + str(cameraNo) + ".txt")

            # create video writer
            #fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            fourcc = cv2.VideoWriter_fourcc(*'MP4V')
            self.writer = cv2.VideoWriter(self.save_video_path, fourcc, 7, (self.im_width + self.infoSpaceWidth, self.im_height))

            # logging
            self.logger.info("Save results to {}".format(self.args.save_path))

        self.max_x = self.im_width  - 1
        self.max_y = self.im_height - 1
        
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print(exc_type, exc_value, exc_traceback)
            
        
    def checkCarList(self, results, idx_frame, fps, frame_count):
        #更新self.carList
        for frame_id, tlwhs, track_ids, class_id in results:
            for tlwh, track_id in zip(tlwhs, track_ids):
                x1, y1, w, h = tlwh
                '''
                x = x1 + w//2
                y = y1 + h//2
                if x > self.max_x:
                    x = self.max_x
                elif x < 0:
                    x = 0
                if y > self.max_y:
                    y = self.max_y
                elif y < 0:
                    y = 0
                '''
                rematchTimes = 10# 避免無窮回圈
                while (track_id, class_id) in self.matchList and rematchTimes > 0:
                    (track_id, class_id) = self.matchList[(track_id, class_id)]
                    rematchTimes -= 1
                
                if (track_id, class_id) in self.carList:
                    #追蹤目標已存在，更新座標
                    car = self.carList[(track_id, class_id)]
                    #car.add(x, y)
                    car.add(x1, y1, w, h, idx_frame)
                    car.lastFrame = idx_frame
                    self.carList[(track_id, class_id)] = car
                    #20210908遮擋車處理
                    #如過loss car 被原本sort機制自動找到，就從 lossCarList 清除
                    if (track_id, class_id) in self.lossCarList:
                        del self.lossCarList[(track_id, class_id)]
                else:
                    
                    car = Car(track_id, class_id)
                    car.add(x1, y1, w, h, idx_frame)
                    car.lastFrame = idx_frame
                    
                    
                    #20210908遮擋車處理
                    #抓不是在畫面邊界出現的車(遮擋車)
                    x = x1 + w // 2
                    y = y1 + h // 2
                    boundW = self.im_width // 8
                    boundH = self.im_height // 8
                    if x > boundW and h > 30: #20211013 camera14 有[距離太遠，導致物體太小而消失]的情況，因此增加物體高度限制
                        if x < self.im_width - boundW:
                            if y > boundH:
                                if y < self.im_height - boundH:
                                    if idx_frame > 7:
                                        self.occurCarList[(track_id, class_id)] = car #這行測試時畫圖用
                                        if len(self.lossCarList.items()) > 0:
                                            #minDistance = sys.maxsize
                                            minDistance = 10000
                                            targetId = (0, 0)
                                            isMatch = False
                                            for key, lossCar in self.lossCarList.items():
                                                if len(lossCar.tracks) > 2 and class_id == lossCar.carType:
                                                    track = lossCar.tracks[len(lossCar.tracks) - 1]
                                                    className = self.class_names[lossCar.carType]
                                                    
                                                    #用外插法估算loss car最新座標
                                                    pretrack = lossCar.tracks[len(lossCar.tracks) - 2]
                                                    diff = track - pretrack
                                                    if diff[4] > 1:
                                                        diff = diff // diff[4]
                                                    frameGap = idx_frame - track[4]
                                                    guessPos = track + diff * frameGap
                                                    guessPosX = guessPos[0] + guessPos[2] // 2
                                                    guessPosY = guessPos[1] + guessPos[3] // 2
                                                    distance = (x - guessPosX) * (x - guessPosX) + (y - guessPosY) * (y - guessPosY)
                                                    if distance < minDistance:
                                                        minDistance = distance
                                                        targetId = key
                                                        isMatch = True
                                            if isMatch:
                                                #print("match %d to %d" % (track_id, targetId[0]) )
                                                self.matchList[(track_id, class_id)] = targetId
                                                del self.lossCarList[targetId]
                                                
                                                '''
                                                print("LossCar List:")
                                                for key, lossCar in self.lossCarList.items():
                                                    print(lossCar.carID)
                                                '''
                    
                    #check match list
                    reMatch = (track_id, class_id)
                    rematchTimes = 10
                    while reMatch in self.matchList and rematchTimes > 10:
                        reMatch = self.matchList[reMatch]
                        rematchTimes -= 1
                    #self.carList[(track_id, class_id)] = car
                    if reMatch in self.carList:
                        car = self.carList[reMatch]
                        car.add(x1, y1, w, h, idx_frame)
                        car.lastFrame = idx_frame
                    
                    #self.carList[(track_id, class_id)] = car
                    self.carList[reMatch] = car                            
                                        
        #判斷有沒有車子離開，定義為[連續5個frame沒出現]為離開
        delList = []
        for key, car in self.carList.items():
            #car = Car(track_id, class_id)
            #(track_id, class_id)
            #20211110 修正，影片結束時，剩下的車也要進行 predict
            if (idx_frame - car.lastFrame > 5 or idx_frame == frame_count) and car.isLeave == False:
                #20211012 改用CNN算路口轉彎
                trunID, roadID = car.predict()
                #trunID, roadID = car.predictWithModel(self.turnModel, self.im_width, self.im_height)
                
                if trunID != -1: #20211110修正，斷掉的(長度小於10)路徑應該直接判定成 isLeave，因為後面還有機會接回來
                    self.carList[key].isLeave = True
                
                '''
                車種定義為
                roads[n][m][0] = 小車
                roads[n][m][1] = 機車
                roads[n][m][2] = 大車
                BigCarID = 1
                SmallCarID = 2
                MotoBikeID = 3
                '''
                #print("predict: car %d, is %d, %d" % ())
                if trunID != -1:
                    carTypeID = 0
                    if car.carType == SmallCarID:
                        carTypeID = 0
                    elif car.carType == MotoBikeID:
                        carTypeID = 1
                    elif car.carType == BigCarID:
                        carTypeID = 2
                    elif car.carType == PeopleID:
                        carTypeID = 3
                    self.roads[roadID][trunID][carTypeID].add((car.carID, car.carType))
                    self.temproads[roadID][trunID][carTypeID].add((car.carID, car.carType))
                    #20211004停等時間計算，統一在車子離開時計算停等時間
                    car.getWaitTimes(fps)
                #else:
                #    print("lossTrack: %d in frame %d " % (track_id, idx_frame))
                #del self.carList[track_id]
                #delList.append(key)
            #20210908遮擋車處理
            #抓不是在畫面邊界消失的車(遮擋車)
            if idx_frame - car.lastFrame == 1:
                track = car.tracks[len(car.tracks) - 1]
                x = track[0] + track[2] // 2
                y = track[1] + track[3] // 2
                boundW = self.im_width // 8
                boundH = self.im_height // 8
                if x > boundW and h > 30: #20211013 camera14 有[距離太遠，導致物體太小而消失]的情況，因此增加物體高度限制
                    if x < self.im_width - boundW:
                        if y > boundH:
                            if y < self.im_height - boundH:
                                #print("ADD: %d %d" % (car.carID, car.carType))
                                self.lossCarList[(car.carID, car.carType)] = car

                
                
                
        #for key in delList:
        #    del self.carList[key]
    
    def saveCarList(self):
        file = open(saveTrackDataPath, 'w')
        maxLength = 0
        for key, car in self.carList.items():
            length = len(car.tracks)
            if maxLength < length:
                maxLength = length
        for key, car in self.carList.items():
            file.write(str(car.carID) + ",")
            file.write(str(car.carType) + ",")
            #用補0補滿的方式讓每筆資料長度一樣
            for i in range(maxLength):
                if i < len(car.tracks):
                    track = car.tracks[i]                
                    file.write(str(track[0]) + ",")
                    file.write(str(track[1]) + ",")
                    file.write(str(track[2]) + ",")
                    file.write(str(track[3]) + ",")
                    if i < maxLength - 1:
                        file.write(str(track[4]) + ",")
                    else:
                        file.write(str(track[4]) + "\n")
                else:
                    if i < maxLength - 1:
                        file.write("0,0,0,0,0,")
                    else:
                        file.write("0,0,0,0,0\n")
        file.close()
        
        file = open(saveTrackDataWithLabelPath, 'w')
        maxLength = 0
        for key, car in self.carList.items():
            length = len(car.tracks)
            if maxLength < length:
                maxLength = length
        for key, car in self.carList.items():
            file.write(str(car.clusterType) + ",")
            file.write(str(car.carID) + ",")
            file.write(str(car.carType) + ",")
            #用補0補滿的方式讓每筆資料長度一樣
            for i in range(maxLength):
                if i < len(car.tracks):
                    track = car.tracks[i]                
                    file.write(str(track[0]) + ",")
                    file.write(str(track[1]) + ",")
                    file.write(str(track[2]) + ",")
                    file.write(str(track[3]) + ",")
                    if i < maxLength - 1:
                        file.write(str(track[4]) + ",")
                    else:
                        file.write(str(track[4]) + "\n")
                else:
                    if i < maxLength - 1:
                        file.write("0,0,0,0,0,")
                    else:
                        file.write("0,0,0,0,0\n")
        file.close()
        return
    
    def test_drawLossCar(self, img, idx_frame):#測試用，把 LossCar 位置畫出來   
        
        #print(len(self.lossCarList.items()))
        for key, car in self.lossCarList.items():
            #print(car.carID)
            if len(car.tracks) > 3:
                track = car.tracks[len(car.tracks) - 1]
                className = self.class_names[car.carType]
                
                x1 = track[0]
                y1 = track[1]
                x2 = track[0] + track[2]
                y2 = track[1] + track[3]
                color = compute_color_for_labels(car.carID)
                label = "Loss%d(%s)" % (car.carID, className)
                t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1.2 , 1)[0]
                cv2.rectangle(img,(x1, y1),(x2,y2),color,3)
                cv2.rectangle(img,(x1, y1),(x1+t_size[0]+3,y1+t_size[1]+4), color,-1)
                cv2.putText(img,label,(x1,y1+t_size[1]+4), cv2.FONT_HERSHEY_PLAIN, 1.2, [255,255,255], 1)
                cv2.circle(img, ((x1+x2)//2, (y1+y2)//2), 3, color, -1)
                
                #用外插法預測loss car最新座標
                pretrack = car.tracks[len(car.tracks) - 3]
                diff = track - pretrack
                if diff[4] > 1:
                    diff = diff // diff[4]
                frameGap = idx_frame - track[4]
                guessPos = track + diff * frameGap
                x1 = guessPos[0]
                y1 = guessPos[1]
                x2 = guessPos[0] + guessPos[2]
                y2 = guessPos[1] + guessPos[3]   
                cv2.rectangle(img,(x1, y1),(x2,y2),color,5)
                
        for key, car in self.occurCarList.items():
            if len(car.tracks) > 1:
                track = car.tracks[len(car.tracks) - 1]
                className = self.class_names[car.carType]
                
                x1 = track[0]
                y1 = track[1]
                x2 = track[0] + track[2]
                y2 = track[1] + track[3]
                color = compute_color_for_labels(car.carID)
                label = "Occur%d(%s)" % (car.carID, className)
                t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1.2 , 1)[0]
                cv2.rectangle(img,(x1, y1),(x2,y2),color,3)
                cv2.rectangle(img,(x1, y1),(x1+t_size[0]+3,y1+t_size[1]+4), color,-1)
                cv2.putText(img,label,(x1,y1+t_size[1]+4), cv2.FONT_HERSHEY_PLAIN, 1.2, [255,255,255], 1)
                cv2.circle(img, ((x1+x2)//2, (y1+y2)//2), 3, color, -1)
        return img
                
    def drawCarList(self, img, idx_frame):
        for key, car in self.carList.items():
            if car.lastFrame == idx_frame:
                if len(car.tracks) > 1:
                    track = car.tracks[len(car.tracks) - 1]
                    className = self.class_names[car.carType]
                    
                    x1 = track[0]
                    y1 = track[1]
                    x2 = track[0] + track[2]
                    y2 = track[1] + track[3]
                    color = compute_color_for_labels(car.carID)
                    label = "%d(%s)" % (car.carID, className)
                    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1.2 , 1)[0]
                    cv2.rectangle(img,(x1, y1),(x2,y2),color,1)
                    cv2.rectangle(img,(x1, y1),(x1+t_size[0]+3,y1+t_size[1]+4), color,-1)
                    cv2.putText(img,label,(x1,y1+t_size[1]+4), cv2.FONT_HERSHEY_PLAIN, 1.2, [255,255,255], 1)
                    cv2.circle(img, ((x1+x2)//2, (y1+y2)//2), 3, color, -1)
                    
        return img

    def run(self):
        results = []
        idx_frame = 0
        
        # 抓影片長度 https://stackoverflow.com/questions/49048111/how-to-get-the-duration-of-video-using-cv2
        fps = self.vdo.get(cv2.CAP_PROP_FPS)
        frame_count = int(self.vdo.get(cv2.CAP_PROP_FRAME_COUNT))
        if fps == 0:
            print("Warning: can't get video fps.... set fps = 7 ")
            fps = 7
        videoLength = frame_count/fps
        jsonData["videoLength"] = videoLength
        
        outputJsonTimes = 1
        totalJsonData = []
        while self.vdo.grab():
            idx_frame += 1
            
            '''
            test_break = 10# 測試用，幾秒後break
            if idx_frame > test_break*7:
                break
            if idx_frame == test_break*7:
                idx_frame = frame_count
            '''
            
            if idx_frame % self.args.frame_interval:
                continue

            start = time.time()
            _, ori_im = self.vdo.retrieve()
            self.ori_im = ori_im
            im = cv2.cvtColor(ori_im, cv2.COLOR_BGR2RGB)
            #if idx_frame == 1:
            #    cv2.imwrite("output/temp.jpg", ori_im)

            #if idx_frame < 80*7:
            #    continue


            # do detection
            bbox_xywh, cls_conf, cls_ids = self.detector(im)

            # select person class
            #mask1 = cls_ids == 2
            #mask2 = cls_ids == 3
            #mask3 = cls_ids == 5
            mask1 = cls_ids == SmallCarID
            mask2 = cls_ids == MotoBikeID
            mask3 = cls_ids == BigCarID
            mask4 = cls_ids == PeopleID
            
            
            
            bbox_xywh1 = bbox_xywh[mask1]
            # bbox dilation just in case bbox too small, delete this line if using a better pedestrian detector
            whRate = 1.2
            bbox_xywh1[:, 3:] *= whRate
            bbox_xywh2 = bbox_xywh[mask2]
            bbox_xywh2[:, 3:] *= whRate
            bbox_xywh3 = bbox_xywh[mask3]
            bbox_xywh3[:, 3:] *= whRate
            bbox_xywh4 = bbox_xywh[mask4]
            bbox_xywh4[:, 3:] *= whRate
            #print(cls_conf)
            #print(cls_ids)
            cls_conf1 = cls_conf[mask1]
            cls_conf2 = cls_conf[mask2]
            cls_conf3 = cls_conf[mask3]
            cls_conf4 = cls_conf[mask4]

            #print(bbox_xywh)
            #print(cls_conf)
            #print(cls_ids)


            # do tracking
            outputs1 = self.deepsort1.update(bbox_xywh1, cls_conf1, im)
            outputs2 = self.deepsort2.update(bbox_xywh2, cls_conf2, im)
            outputs3 = self.deepsort3.update(bbox_xywh3, cls_conf3, im)
            outputs4 = self.deepsort4.update(bbox_xywh4, cls_conf4, im)
            
            #測試結果，deepsort要的xywh，x和y是中心點座標，不是左上角
            '''
            im2 = ori_im.copy()
            for box in bbox_xywh:
                #print(box)
                x1 = int(box[0] - box[2] / 2)
                y1 = int(box[1] - box[3] / 2)
                x2 = int(box[0] + box[2] / 2)
                y2 = int(box[1] + box[3] / 2)
                x1 = int(x1)
                x2 = int(x2)
                y1 = int(y1)
                y2 = int(y2)
                #print("=====")
                #print(x1)
                #print(x2)
                #print(y1)
                #print(y2)
                cv2.rectangle(ori_im, (x1, y1), (x2, y2), (0, 0, 255), 1, cv2.LINE_AA)
            #cv2.imshow("temp", im2)
            #cv2.waitKey(0)
            '''

            # draw boxes for visualization
            outputs_one_frame = []
            outputs = outputs1
            classID = SmallCarID
            if len(outputs) > 0:
                bbox_tlwh = []
                bbox_xyxy1 = outputs[:, :4]
                identities = outputs[:, -1]
                #ori_im = draw_boxes2(ori_im, bbox_xyxy1, identities, self.class_names[classID])

                for bb_xyxy in bbox_xyxy1:
                    bbox_tlwh.append(self.deepsort1._xyxy_to_tlwh(bb_xyxy))

                results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))


            outputs = outputs2
            classID = MotoBikeID
            if len(outputs) > 0:
                bbox_tlwh = []
                bbox_xyxy2 = outputs[:, :4]
                identities = outputs[:, -1]
                #ori_im = draw_boxes2(ori_im, bbox_xyxy2, identities, self.class_names[classID])

                for bb_xyxy in bbox_xyxy2:
                    bbox_tlwh.append(self.deepsort2._xyxy_to_tlwh(bb_xyxy))

                results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                
            outputs = outputs3
            classID = BigCarID
            if len(outputs) > 0:
                bbox_tlwh = []
                bbox_xyxy3 = outputs[:, :4]
                identities = outputs[:, -1]
                #ori_im = draw_boxes2(ori_im, bbox_xyxy3, identities, self.class_names[classID])

                for bb_xyxy in bbox_xyxy3:
                    bbox_tlwh.append(self.deepsort3._xyxy_to_tlwh(bb_xyxy))

                results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                
                
            outputs = outputs4
            classID = PeopleID
            if len(outputs) > 0:
                bbox_tlwh = []
                bbox_xyxy4 = outputs[:, :4]
                identities = outputs[:, -1]
                for bb_xyxy in bbox_xyxy4:
                    bbox_tlwh.append(self.deepsort4._xyxy_to_tlwh(bb_xyxy))
                results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                
                
                
            #self.checkArea(ori_im, outputs_one_frame)
            self.checkCarList(outputs_one_frame, idx_frame, fps, frame_count)
            ori_im = self.drawCarList(ori_im, idx_frame)
            
            #ori_im = self.test_drawLossCar(ori_im, idx_frame)
            
            #show(self.trackMap, "tracks")
            #show(ori_im, "videos")
            #cv2.waitKey(15)
            
            #畫熱區螢幕太亂...改為只畫前100 frame
            #20210728修改熱區判定方式，目前比較不亂可以畫
            #if idx_frame < 100:
            #ori_im = cv2.add(self.hotAreasMat, ori_im)
            
            
            new_im = np.zeros((ori_im.shape[0], ori_im.shape[1] + self.infoSpaceWidth, ori_im.shape[2]), np.uint8)
            new_im[0:ori_im.shape[0], 0:ori_im.shape[1], :] = ori_im;
            ori_im = new_im
            
            textColor = [0, 0, 0]
            textColorB = [0, 255, 255]
            textSize = 1
            
            tx = self.im_width + 10
            ty = 20
            
            px = tx
            py = ty
            text = "Road 1(Right in):"
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "Small Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[0][2][0])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[0][0][0])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[0][1][0])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[0][4][0])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            
            px = tx
            py = ty + 80
            text = "Big Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[0][2][2])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[0][0][2])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[0][1][2])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[0][4][2])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            px = tx
            py = ty + 160
            text = "Motorbike:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[0][2][1])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[0][0][1])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[0][1][1])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[0][4][1])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "twoPartLeft: %d"  % len(self.roads[0][3][1])
            cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            
            tx = self.im_width + 10
            ty = 300
            px = tx
            py = ty
            text = "Road 2(Left in):"
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "Small Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[1][2][0])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[1][0][0])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[1][1][0])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[1][4][0])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            
            px = tx
            py = ty + 80
            text = "Big Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[1][2][2])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[1][0][2])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[1][1][2])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[1][4][2])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            px = tx
            py = ty + 160
            text = "Motorbike:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[1][2][1])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[1][0][1])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[1][1][1])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[1][4][1])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "twoPartLeft: %d"  % len(self.roads[1][3][1])
            cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            

            tx = self.im_width + 180
            ty = 20
            px = tx
            py = ty
            text = "Road 3(Up in):"
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "Small Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[2][2][0])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[2][0][0])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[2][1][0])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[2][4][0])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            px = tx
            py = ty + 80
            text = "Big Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[2][2][2])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[2][0][2])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[2][1][2])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[2][4][2])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            px = tx
            py = ty + 160
            text = "Motorbike:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[2][2][1])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[2][0][1])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[2][1][1])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[2][4][1])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "twoPartLeft: %d"  % len(self.roads[2][3][1])
            cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            tx = self.im_width + 180
            ty = 300
            px = tx
            py = ty
            text = "Road 4(Down in):"
            cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "Small Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[3][2][0])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[3][0][0])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[3][1][0])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[3][4][0])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            px = tx
            py = ty + 80
            text = "Big Car:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[3][2][2])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[3][0][2])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[3][1][2])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[3][4][2])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            px = tx
            py = ty + 160
            text = "Motorbike:"
            cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnRight: %d"  % len(self.roads[3][2][1])
            cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "straight: %d"  % len(self.roads[3][0][1])
            cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnLeft: %d"  % len(self.roads[3][1][1])
            cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "turnBack: %d"  % len(self.roads[3][4][1])
            cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            text = "twoPartLeft: %d"  % len(self.roads[3][3][1])
            cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            

            

            end = time.time()
            
            
            #cv2.imshow("test", ori_im)
            #cv2.waitKey(0)

            if self.args.display:
                cv2.imshow("test", ori_im)
                cv2.waitKey(1)

            if self.args.save_path:
                self.writer.write(ori_im)
                

            # save results
            write_results(self.save_results_path, results, 'mot')
            

            # logging
            self.logger.info("time: {:.03f}s, fps: {:.03f}, detection numbers: {}, tracking numbers: {}" \
                             .format(end - start, 1 / (end - start), bbox_xywh.shape[0], len(outputs_one_frame)))
                
            #20211015 依會議討論交通量改為每分鐘輸出
            if (idx_frame % (fps * 60) == 0) or (idx_frame == frame_count):
                #20211014 依昨天會議討論，改為每分鐘輸出一次json
                #把交通量寫入json
                #standardCounting
                bigCarNum = 0
                smallCarNum = 0
                motorbikeNum = 0
                peopleNum = 0
                for key, car in self.carList.items():
                    if car.isCounted:
                        continue
                    if car.carType == SmallCarID:
                        smallCarNum += 1
                    elif car.carType == MotoBikeID:
                        motorbikeNum += 1
                    elif car.carType == BigCarID:
                        bigCarNum += 1
                    elif car.carType == PeopleID:
                        peopleNum += 1
                jsonData["standardCounting"]["bigCarNum"] = bigCarNum
                jsonData["standardCounting"]["smallCarNum"] = smallCarNum
                jsonData["standardCounting"]["motorbikeNum"] = motorbikeNum
                jsonData["standardCounting"]["peopleNum"] = peopleNum
                
                #turnCounting
                #應該有4個路口，右 straight_R 左 straight_L 上 cross_U 下 cross_D
                #20211012 增加迴轉車 turnBack
                jsonData["turnCounting"]["straight_R"]["smallCar"]["straight"] = len(self.temproads[0][0][0])
                jsonData["turnCounting"]["straight_R"]["smallCar"]["turnLeft"] = len(self.temproads[0][1][0])
                jsonData["turnCounting"]["straight_R"]["smallCar"]["turnRight"] = len(self.temproads[0][2][0])
                jsonData["turnCounting"]["straight_R"]["smallCar"]["turnBack"] = len(self.temproads[0][4][0])         
                jsonData["turnCounting"]["straight_R"]["motorbike"]["straight"] = len(self.temproads[0][0][1])
                jsonData["turnCounting"]["straight_R"]["motorbike"]["turnLeft"] = len(self.temproads[0][1][1])
                jsonData["turnCounting"]["straight_R"]["motorbike"]["turnRight"] = len(self.temproads[0][2][1])
                jsonData["turnCounting"]["straight_R"]["motorbike"]["turnBack"] = len(self.temproads[0][4][1])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["straight"] = len(self.temproads[0][0][2])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["turnLeft"] = len(self.temproads[0][1][2])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["turnRight"] = len(self.temproads[0][2][2])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["turnBack"] = len(self.temproads[0][4][2])
                
                jsonData["turnCounting"]["straight_L"]["smallCar"]["straight"] = len(self.temproads[1][0][0])
                jsonData["turnCounting"]["straight_L"]["smallCar"]["turnLeft"] = len(self.temproads[1][1][0])
                jsonData["turnCounting"]["straight_L"]["smallCar"]["turnRight"] = len(self.temproads[1][2][0])
                jsonData["turnCounting"]["straight_L"]["smallCar"]["turnBack"] = len(self.temproads[1][4][0])        
                jsonData["turnCounting"]["straight_L"]["motorbike"]["straight"] = len(self.temproads[1][0][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["turnLeft"] = len(self.temproads[1][1][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["turnRight"] = len(self.temproads[1][2][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["turnBack"] = len(self.temproads[1][4][1]) 
                jsonData["turnCounting"]["straight_L"]["bigCar"]["straight"] = len(self.temproads[1][0][2])
                jsonData["turnCounting"]["straight_L"]["bigCar"]["turnLeft"] = len(self.temproads[1][1][2])
                jsonData["turnCounting"]["straight_L"]["bigCar"]["turnRight"] = len(self.temproads[1][2][2])
                jsonData["turnCounting"]["straight_L"]["bigCar"]["turnBack"] = len(self.temproads[1][4][2]) 
                
                jsonData["turnCounting"]["cross_U"]["smallCar"]["straight"] = len(self.temproads[2][0][0])
                jsonData["turnCounting"]["cross_U"]["smallCar"]["turnLeft"] = len(self.temproads[2][1][0])
                jsonData["turnCounting"]["cross_U"]["smallCar"]["turnRight"] = len(self.temproads[2][2][0])
                jsonData["turnCounting"]["cross_U"]["smallCar"]["turnBack"] = len(self.temproads[2][4][0])        
                jsonData["turnCounting"]["cross_U"]["motorbike"]["straight"] = len(self.temproads[2][0][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["turnLeft"] = len(self.temproads[2][1][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["turnRight"] = len(self.temproads[2][2][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["turnBack"] = len(self.temproads[2][4][1])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["straight"] = len(self.temproads[2][0][2])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["turnLeft"] = len(self.temproads[2][1][2])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["turnRight"] = len(self.temproads[2][2][2])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["turnBack"] = len(self.temproads[2][4][2])
                      
                jsonData["turnCounting"]["cross_D"]["smallCar"]["straight"] = len(self.temproads[3][0][0])
                jsonData["turnCounting"]["cross_D"]["smallCar"]["turnLeft"] = len(self.temproads[3][1][0])
                jsonData["turnCounting"]["cross_D"]["smallCar"]["turnRight"] = len(self.temproads[3][2][0])   
                jsonData["turnCounting"]["cross_D"]["smallCar"]["turnBack"] = len(self.temproads[3][4][0])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["straight"] = len(self.temproads[3][0][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["turnLeft"] = len(self.temproads[3][1][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["turnRight"] = len(self.temproads[3][2][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["turnBack"] = len(self.temproads[3][4][1])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["straight"] = len(self.temproads[3][0][2])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["turnLeft"] = len(self.temproads[3][1][2])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["turnRight"] = len(self.temproads[3][2][2])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["turnBack"] = len(self.temproads[3][4][2])
                
                #20211012 兩段式左轉
                jsonData["turnCounting"]["straight_R"]["motorbike"]["hookTurn"] = len(self.temproads[0][3][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["hookTurn"] = len(self.temproads[1][3][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["hookTurn"] = len(self.temproads[2][3][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["hookTurn"] = len(self.temproads[3][3][1])
                
                
                
                #TODO accelerationCounting
                
                
                #20211005 停等時間
                #20211006 更新，平均停等時間似乎還要依照車種分開計算
                #20211124 更新，平均停等時間還得依轉向分開
                '''
                waitTime_R = [0, 0, 0, 0]
                waitTime_L = [0, 0, 0, 0]
                waitTime_U = [0, 0, 0, 0]
                waitTime_D = [0, 0, 0, 0]
                carNum_R = [0, 0, 0, 0]
                carNum_L = [0, 0, 0, 0]
                carNum_U = [0, 0, 0, 0]
                carNum_D = [0, 0, 0, 0]
                tar = 0
                for key, car in self.carList.items():
                    if car.isCounted:
                        continue
                    if car.carType == SmallCarID:
                        tar = 0
                    elif car.carType == MotoBikeID:
                        tar = 1
                    elif car.carType == BigCarID:
                        tar = 2
                    elif car.carType == PeopleID:
                        tar = 3
                        
                    if car.roadID == 0:
                        carNum_R[tar] += 1
                        waitTime_R[tar] += car.maxWaitSec
                    elif car.roadID == 1:
                        carNum_L[tar] += 1
                        waitTime_L[tar] += car.maxWaitSec
                    elif car.roadID == 2:
                        carNum_U[tar] += 1
                        waitTime_U[tar] += car.maxWaitSec
                    elif car.roadID == 3:
                        carNum_D[tar] += 1
                        waitTime_D[tar] += car.maxWaitSec
                    self.carList[key].isCounted = True
                    
                for i in range(4):
                    if carNum_R[i] > 0:
                        waitTime_R[i] /= carNum_R[i]
                    if carNum_L[i] > 0:
                        waitTime_L[i] /= carNum_L[i]
                    if carNum_U[i] > 0:
                        waitTime_U[i] /= carNum_U[i]
                    if carNum_D[i] > 0:
                        waitTime_D[i] /= carNum_D[i]
                
                jsonData["turnCounting"]["straight_R"]["smallCar"]["averageStoppedDelayTime"] = waitTime_R[0]
                jsonData["turnCounting"]["straight_R"]["bigCar"]["averageStoppedDelayTime"] = waitTime_R[1]
                jsonData["turnCounting"]["straight_R"]["motorbike"]["averageStoppedDelayTime"] = waitTime_R[2]
                
                jsonData["turnCounting"]["straight_L"]["smallCar"]["averageStoppedDelayTime"] = waitTime_L[0]
                jsonData["turnCounting"]["straight_L"]["bigCar"]["averageStoppedDelayTime"] = waitTime_L[1]
                jsonData["turnCounting"]["straight_L"]["motorbike"]["averageStoppedDelayTime"] = waitTime_L[2]
                
                jsonData["turnCounting"]["cross_U"]["smallCar"]["averageStoppedDelayTime"] = waitTime_U[0]
                jsonData["turnCounting"]["cross_U"]["bigCar"]["averageStoppedDelayTime"] = waitTime_U[1]
                jsonData["turnCounting"]["cross_U"]["motorbike"]["averageStoppedDelayTime"] = waitTime_U[2]
                
                jsonData["turnCounting"]["cross_D"]["smallCar"]["averageStoppedDelayTime"] = waitTime_D[0]
                jsonData["turnCounting"]["cross_D"]["bigCar"]["averageStoppedDelayTime"] = waitTime_D[1]
                jsonData["turnCounting"]["cross_D"]["motorbike"]["averageStoppedDelayTime"] = waitTime_D[2]
                '''
                
                
                
                waitTimes = resetWaitCounting()
                carNums = resetWaitCounting()
                '''
                WaitCounting順序同self.roads:
                第一層路口:右、左、上、下
                第二層轉向:直、左、右、兩、迴
                第三層車種:小車、機車、大車、行人
                '''
                
                tarRoadType = 0
                tarTurnType = 0
                tarCarType = 0
                tarRoadType_toJson = ["straight_R", "straight_L", "cross_U", "cross_D"]
                tarCarType_toJson = ["smallCar", "motorbike", "bigCar", "people"]
                tarTurnType_toJson = ["averageStoppedDelayTime_Straight", "averageStoppedDelayTime_TurnLeft", 
                                      "averageStoppedDelayTime_TurnRight", "averageStoppedDelayTime_HookTurn", 
                                      "averageStoppedDelayTime_TurnBack"]
                for key, car in self.carList.items():
                    if car.isCounted:
                        continue
                    if car.carType == SmallCarID:
                        tarCarType = 0
                    elif car.carType == MotoBikeID:
                        tarCarType = 1
                    elif car.carType == BigCarID:
                        tarCarType = 2
                    elif car.carType == PeopleID:
                        tarCarType = 3
                        
                    tarRoadType = car.roadID
                    tarTurnType = car.turnID
                    
                    if tarRoadType == -1 or tarTurnType == -1:
                        continue
                    
                    waitTimes[tarRoadType][tarTurnType][tarCarType] += car.maxWaitSec
                    carNums[tarRoadType][tarTurnType][tarCarType] += 1

                    self.carList[key].isCounted = True
                

                for i in range(len(waitTimes)):
                    for j in range(len(waitTimes[i])):
                        for k in range(len(waitTimes[i][j])):
                            if carNums[i][j][k] > 0:
                                waitTimes[i][j][k] /= carNums[i][j][k]
                
                for i in range(len(waitTimes)):
                    for j in range(len(waitTimes[i])):
                        for k in range(len(waitTimes[i][j])):
                            #路口i，轉向j，車種k
                            #機車以外沒有HookTurn
                            if k != 1:
                                if j == 4:
                                    continue
                            jsonData["turnCounting"][tarRoadType_toJson[i]][tarCarType_toJson[k]][tarTurnType_toJson[j]] = waitTimes[i][j][k]


                #sqlite_insert(jsonData, outputJsonTimes)
                
                #{"minute":1,"data":{camera........},{"minute":2,"data":{camera........}
                tempJson = {}
                tempJson["minute"] = outputJsonTimes
                tempJson["data"] = copy.deepcopy(jsonData)
                outputJsonTimes += 1
                totalJsonData.append(tempJson)
                self.temproads = resetRoadCounting()
        
        #for target in self.turnRightCar:
        #    print(target)
        #self.countLossTracks()
        
        
        
        #20211109暫時使用：從輸出影片看哪輛車被判定成轉哪邊有難度(在車子多時，看不出哪輛車被判定往哪轉)，把路徑畫出來看
        '''
        trun =["straight", "turnLeft", "turnRight", "hookTurn", "turnBack", "Unknow"]
        road =["R", "L", "U", "D", "Unknow"]
        backgroundImg = cv2.imread("output/temp%s.png" % cameraNo)
        for key, car in self.carList.items():
            tracks = car.tracks;
            #trackMap = np.zeros((self.im_height, self.im_width, 3), np.uint8)
            trackMap = backgroundImg.copy()
            pxc = 0
            pyc = 0
            for i in range(len(tracks)):
                track = tracks[i]
                xc = track[0] + track[2] // 2
                yc = track[1] + track[3] // 2
                if i == 0:
                    cv2.circle(trackMap, (xc, yc), 20, (255, 0, 0), -1)
                else:
                    cv2.line(trackMap, (xc, yc), (pxc, pyc), (255, 0, 0), 3)
                cv2.circle(trackMap, (xc, yc), 5, (0, 0, 255), -1)
                pxc = xc
                pyc = yc
            cv2.putText(trackMap, "Road=%s" % (road[car.roadID]) , (50, 50), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 2)
            cv2.putText(trackMap, "Turn=%s" % (trun[car.turnID]) , (50, 100), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 2)
            cv2.imwrite("output/temp/%d_%d.png" % (car.carID, car.carType), trackMap)
        '''
        
        self.saveCarList()
        
        #20211123路口資訊寫出到excel
        wb = load_workbook(excel_format_file_path)
        excel_save_path = "output/camera%d_%s.xlsx" % (cameraNo, videoStartTime_ori)
        sheet = wb["sheet"]
        
        #以下這5行來自 https://stackoverflow.com/questions/38734044/losing-merged-cells-border-while-editing-excel-file-with-openpyxl/46757594
        #修正框線格式遺失問題
        for merged_cells in sheet.merged_cells.ranges:
            style = sheet.cell(merged_cells.min_row, merged_cells.min_col)._style
            for col in range(merged_cells.min_col, merged_cells.max_col + 1):
                for row in range(merged_cells.min_row, merged_cells.max_row + 1): 
                    sheet.cell(row, col)._style = style
        
        
        #抓全部左轉/右轉/直行 的 大車/小車/機車/行人，依路口分開

        #這邊輸出轉彎量
        targetRoad = 0 #Right in
        targetTurnType = 0#straight
        targetCarType = 0 #smallCar
        
        #行人/機車/小客車/大客車 對應row為 K L M N ，然後 self.roads 順序是小車/機車/大車/行人，弄個轉換表
        carType_toExcel = ["M", "L", "N", "K"]
        #同理，路口轉換對應右左上下分別轉5, 11, 14, 8
        roadType_toExcel = [5, 11, 14, 8]
        #同理，轉彎轉換，直左右兩迴，對應 1, 0, 2, X, X
        turnType_toExcel = [1, 0, 2, -1, -1]
        
        for targetRoad in range(len(self.roads)):
            for targetTurnType in range(len(self.roads[targetRoad])):
                for targetCarType in range(len(self.roads[targetRoad][targetTurnType])):
                    countNum = len(self.roads[targetRoad][targetTurnType][targetCarType])
                    #機車要處理兩段式左轉
                    if targetCarType == 2:#如果是機車，把兩段式左轉的count也加上去
                        countNum += len(self.roads[targetRoad][3][targetCarType])
                    #然後excel表只要輸出直行/左轉/右轉
                    if targetTurnType <= 2:
                        sheet["%s%d" % (carType_toExcel[targetCarType], roadType_toExcel[targetRoad] 
                                        + turnType_toExcel[targetTurnType])] = countNum
                        
        
        #路口停等時間輸出excel

        waitTimes = resetWaitCounting()
        carNums = resetWaitCounting()
        '''
        WaitCounting順序同self.roads:
        第一層路口:右、左、上、下
        第二層轉向:直、左、右、兩、迴
        第三層車種:小車、機車、大車、行人
        '''
        
        tarRoadType = 0
        tarTurnType = 0
        tarCarType = 0
        #車種轉excel對應row更換，路口/轉彎對應相同
        carType_toExcel = ["I", "H", "J", "G"]
        
        for key, car in self.carList.items():
            if car.carType == SmallCarID:
                tarCarType = 0
            elif car.carType == MotoBikeID:
                tarCarType = 1
            elif car.carType == BigCarID:
                tarCarType = 2
            elif car.carType == PeopleID:
                tarCarType = 3
                
            tarRoadType = car.roadID
            tarTurnType = car.turnID
            
            if tarRoadType == -1 or tarTurnType == -1:
                continue  
            waitTimes[tarRoadType][tarTurnType][tarCarType] += car.maxWaitSec
            carNums[tarRoadType][tarTurnType][tarCarType] += 1
        
        for i in range(len(waitTimes)):
            for j in range(len(waitTimes[i])):
                for k in range(len(waitTimes[i][j])):
                    if carNums[i][j][k] > 0:
                        waitTimes[i][j][k] /= carNums[i][j][k]

        for targetRoad in range(len(waitTimes)):
            for targetTurnType in range(len(waitTimes[targetRoad])):
                for targetCarType in range(len(waitTimes[targetRoad][targetTurnType])):
                    #excel表只要輸出直行/左轉/右轉
                    if targetTurnType <= 2:
                        targetValue = waitTimes[targetRoad][targetTurnType][targetCarType]
                        sheet["%s%d" % (carType_toExcel[targetCarType], roadType_toExcel[targetRoad] 
                                        + turnType_toExcel[targetTurnType])] = targetValue
                        
        #抓上午/下午/尖峰/離峰 2021-09-20-09-00-00
        #videoStartTime_ori
        time_hour = int(videoStartTime_ori.split("-")[3])
        tempString = ""
        if time_hour < 12:
            tempString = "上午"
        else:
            tempString = "下午"
        #尖峰時段定義為8點到10點與18點到20點
        if time_hour in [8,9,10,18,19,20]:
            tempString += "尖峰"
        else:
            tempString += "離峰"
        sheet["B3"] = tempString
        
        
        #輸出路口，從camera編號對應
        roadName = "camera%d" % cameraNo
        if cameraNo == 3:
            sheet["C6"] = "中正路"
            sheet["B8"] = "大興西路"
            roadName = "中正大興路口"
        elif cameraNo == 14:
            sheet["C6"] = "中正路"
            sheet["B8"] = "同德六街"
            roadName = "中正同德六街路口"
        sheet.title = "%s(%s)" % (roadName, tempString)
        
        wb.save(excel_save_path)
        
        
        #excel輸出結束
        
        
        sqlite_insert(totalJsonData)
        cv2.destroyAllWindows()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("VIDEO_PATH", type=str)
    parser.add_argument("--config_detection", type=str, default="./configs/yolor.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    # parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
    parser.add_argument("--display", action="store_true")
    parser.add_argument("--frame_interval", type=int, default=1)
    parser.add_argument("--display_width", type=int, default=800)
    parser.add_argument("--display_height", type=int, default=600)
    parser.add_argument("--save_path", type=str, default="./output/")
    parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
    parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
    return parser.parse_args()


def resetRoadCounting():
    roads = []
    for i in range(4):#雙向道分開算，有四條道路
        roads.append([])
    for i in range(len(roads)):
        for j in range(5):#轉彎有5種狀況
            roads[i].append([])
    for i in range(len(roads)):
        for j in range(len(roads[i])):
            for k in range(4):#大車小車機車行人，有四種
                roads[i][j].append(set())
    return roads

def resetWaitCounting():
    roads = []
    for i in range(4):#雙向道分開算，有四條道路
        roads.append([])
    for i in range(len(roads)):
        for j in range(5):#轉彎有5種狀況
            roads[i].append([])
    for i in range(len(roads)):
        for j in range(len(roads[i])):
            for k in range(4):#大車小車機車行人，有四種
                roads[i][j].append(0)
    return roads

def reset_jeonData():
    '''
    20211124 停等時間格式更新：
    對照要求輸出的excel格式，停等時間也得依左轉/右轉/直行分開統計
    原本
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "averageStoppedDelayTime": 0 },
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "averageStoppedDelayTime": 0 },
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, "averageStoppedDelayTime": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "averageStoppedDelayTime": 0 }
    
    改為
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0, "averageStoppedDelayTime_HookTurn": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0,
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0}
    '''
    global jsonData
    jsonData = {
        "cameraNo":0, 
        "videoStartTime": "2000-00-00 00:00:00", 
        "videoLength":0, 
        "type": 0,
    
    "standardCounting" : {
    
    "bigCarNum": 0, "smallCarNum": 0, "motorbikeNum": 0, "peopleNum": 0
    
    },
    
    "turnCounting": {
    
    "straight_R": {
    
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0, "averageStoppedDelayTime_HookTurn": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0,
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0}
    
    },
    
    "straight_L": {
    
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0, "averageStoppedDelayTime_HookTurn": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0,
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0}
    
    },
    
    "cross_U": {
    
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0, "averageStoppedDelayTime_HookTurn": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0,
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0}
    
    },
    
    "cross_D": {
    
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0, "averageStoppedDelayTime_HookTurn": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0,
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0}
    
    }
    
    },
    
    "accelerationCounting": {
    
    "bigCar": { "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0 },
    
    "smallCar": {  "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0},
    
    "motorbike": { "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0}
    
    },
    
    "extraCounting": {
    }
    
    }
    return

def sqlite_createTable():
    #交通量輸出到sqlite
    conn = sqlite3.connect('data/db/data.db')
    
    #Table不存在則建立 https://stackoverflow.com/questions/4098008/create-table-in-sqlite-only-if-it-doesnt-exist-already
    #目前Table定義為 NAME,RECORD,CAMERA_NO,TYPE ，NAME為主鍵    
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS `RECORD`
       (`NAME`          CHAR(64) PRIMARY KEY   NOT NULL,
       `RECORD`         TEXT    NOT NULL,
       `CAMERA_NO`      INT     NOT NULL,
       `TYPE`           INT     NOT NULL);''')
    conn.commit()
    conn.close()
    return

def sqlite_insert(jsonData):
    
    conn = sqlite3.connect('data/db/data.db')
    dataOut = json.dumps(jsonData)
    #輸出到Table
    name = "CAMERA_" + str(cameraNo) + "_" + videoStartTime_ori
    #insert or update 語法參照 https://stackoverflow.com/questions/3634984/insert-if-not-exists-else-update
    c = conn.cursor()
    sqlstr = 'INSERT OR REPLACE INTO RECORD VALUES (\'%s\', \'%s\', %d, %d);' % (name, dataOut, cameraNo, jsonData[0]["data"]["type"])
    #print(sqlstr)
    c.execute(sqlstr)
    conn.commit()
    conn.close()
    
    return

'''
測試結果：分類特徵用[進入點座標,離開點座標,轉彎向量,進入向量]效果比較好
然後分類12類時，會有部分車道分成同一類
分類16類分得比較開
各車道分類狀況(手動觀察，影像開頭和結尾路徑不完全的不計算)；
上面進，直行：大多6，3輛3、2輛15
上面進，左轉：大多14，1輛15
上面進，右轉：9

下面進，直行：4
下面進，左轉：大多1，2輛13，1輛5，1輛7
下面進，右轉：螢幕外

左邊進，直行： 10 5 8
左邊進，左轉：大多0 ，一輛12 
左邊進，右轉：螢幕外

右邊進，直行：大多 13 7，2輛1，在6:40有公車遮擋時約10輛分類為2
右邊進，左轉：3
右邊進，兩段式左轉：3 6 各一輛
右邊進，右轉：11 4


特殊路徑：
上面迴轉車：0
小巷子鑽出來：9

0:左邊進，左轉
1:下面進，左轉
2:公車遮擋時的右邊進直行
3:右邊進，左轉，上面進直行3輛，兩段式左轉1輛
4:下面進，直行，右邊進，右轉    -->類別混淆了
5:左邊進，直行，下面進左轉1輛
6:上面進，直行，兩段式左轉1輛
7:右邊進，直行，下面進左轉1輛
8:左邊進，直行
9:上面進，右轉
10:左邊進，直行
11:右邊進，右轉
12:左邊進左轉1輛
13:右邊進，直行，下面進左轉2輛
14:上面進，左轉
15:上面進直行2輛、上面進左轉1輛

混淆1類，抓錯10個

嘗試訓練分類用模型
依照 https://medium.com/%E7%94%A8%E5%8A%9B%E5%8E%BB%E6%84%9B%E4%B8%80%E5%80%8B%E4%BA%BA%E7%9A%84%E8%A9%B1-%E5%BF%83%E4%B9%9F%E6%9C%83%E7%97%9B%E7%9A%84/%E9%BB%98%E9%BB%98%E5%9C%B0%E5%AD%B8-deep-learning-7-%E9%80%8F%E9%81%8Ekeras%E9%80%B2%E8%A1%8C%E9%9D%9E%E7%9B%A3%E7%9D%A3%E5%BC%8F%E5%88%86%E7%BE%A4-3afb57ea990c 
的方法

他的 autoencoder 模型比較單純(一維，只有全連接)，改成2維卷積
最後面一層 ClusteringLayer 直接沿用，未修改

input 結構多次嘗試，
直接輸入tracking到的座標點效果很差
把tracking到的座標點連成線，畫成軌跡圖，效果好多了，但[上面路口進，左轉]與[下面路口進，直行]軌跡太相近，會分類錯誤
再加上向量概念，輸入軌跡塗改成彩色，B通道是原本軌跡，G通道是軌跡向量的X分量，R通道是Y分量，[上面路口進，左轉]與[下面路口進，直行]可區分了

各車道分類狀況(手動觀察，影像開頭和結尾路徑不完全得不計算)；
上面進，直行：9或1 ->靠左方車道的
上面進，左轉：1幾乎都被分類成1
上面進，右轉：15

下面進，直行：大多 11 3 ，2輛12
下面進，左轉：大多 4，1輛0
下面進，右轉：螢幕外

左邊進，直行：6 13 2
左邊進，左轉：8
左邊進，右轉：螢幕外

右邊進，直行：7 5，1輛0，在6:40有公車遮擋時約10輛分類為14
右邊進，左轉：15
右邊進，兩段式左轉：14 9
右邊進，右轉：10


特殊路徑：
上面迴轉車：12
小巷子鑽出來：12

0:下面進，左轉或右邊進，直行(各1輛)
1:上面進，左轉或上面進，直行  -->類別混淆了
2:左邊進，直行
3:下面進，直行
4:下面進，左轉
5:右邊進，直行
6:左邊進，直行
7:右邊進，直行
8:左邊進，左轉
9:右邊進，兩段式左轉
10:右邊進，右轉
11:下面進，直行
12:下面進，直行2輛、上面迴轉車、小巷子鑽出來
13:左邊進，直行
14:公車遮擋的右邊進，直行或右邊進，兩段式左轉1輛
15:上面進，右轉或右邊進，左轉  -->類別混淆了
共兩個類別混淆

混淆2類，抓錯5個


嘗試分到幾類才能把[上面進，左轉][上面進，直行]類別混淆的去除：
20類沒用
30類沒用
40類沒用
50類沒用
這兩類路徑太像，增加類別沒效果


=======
接下來對camera14實驗
首先嘗試模型沒重train直接對camera14分類
狀況不好，蠻多車道發生混淆，重新train

重train後:
    
上面進，直行：15 1
上面進，左轉：3(只有2輛)
上面進，右轉：10
上面進，迴轉：9

下面進，直行：2 5 12 
下面進，左轉：10
下面進，右轉：3 4

左邊進，直行：3
左邊進，左轉：9
左邊進，右轉：14 1

右邊進，直行：8
右邊進，左轉：6
右邊進，右轉：11
還是好多類別混淆

改用直接 sklearn 分類試試
上面進，直行：8  10*1
上面進，左轉：9
上面進，右轉：15
上面進，迴轉：10或9
上面進，兩段式左轉：9

下面進，直行：5 13 7*1
下面進，左轉：14
下面進，右轉：7

左邊進，直行：4 3*3
左邊進，左轉：3 4*5
左邊進，右轉：12

右邊進，直行：6
右邊進，左轉：6
右邊進，右轉：1 10*1
稍微好一些，但還是有混淆

測試結果：由於有些車道路徑太相似(例如，camera3的上方進直行與上方進左轉，非監督式學習會分成同一類，
所以可能還是要用監督式學習比較可行

'''

if __name__ == "__main__":
    '''
    car = Car(0, 2)
    car.add(50, 100)
    car.add(50, 98)
    car.add(50, 95)
    car.add(50, 90)
    car.add(48, 85)
    car.add(46, 80)
    car.add(42, 70)
    car.add(40, 60)
    car.add(35, 55)
    car.add(30, 50)
    car.add(20, 50)
    car.add(10, 50)
    car.add(0, 50)
    r1 = car.predict()
    print(r1)
    
    
    car = Car(1, 2)
    car.add(50, 100)
    car.add(50, 98)
    car.add(50, 95)
    car.add(50, 90)
    car.add(48, 85)
    car.add(46, 80)
    car.add(48, 70)
    car.add(50, 60)
    car.add(51, 50)
    car.add(50, 40)
    car.add(50, 30)
    car.add(50, 20)
    car.add(50, 10)
    r1 = car.predict()
    print(r1)

    car = Car(2, 2)
    car.add(50, 100)
    car.add(50, 98)
    car.add(50, 95)
    car.add(50, 90)
    car.add(52, 85)
    car.add(58, 80)
    car.add(60, 70)
    car.add(65, 60)
    car.add(72, 55)
    car.add(80, 50)
    car.add(85, 50)
    car.add(90, 50)
    car.add(100, 50)
    r1 = car.predict()
    print(r1)
    
    
    car = Car(3, 2)
    car.add(50, 100)
    car.add(52, 98)
    car.add(54, 90)
    car.add(60, 80)
    car.add(62, 70)
    car.add(68, 60)
    car.add(70, 50)
    car.add(75, 60)
    car.add(77, 70)
    car.add(80, 80)
    car.add(80, 90)
    car.add(80, 95)
    car.add(40, 100)
    r1 = car.predict()
    print(r1)
    '''

    
    args = parse_args()
    cfg = get_config()
    cfg.merge_from_file(args.config_detection)
    cfg.merge_from_file(args.config_deepsort)



    reset_jeonData()
    readSetting(args)
    

    sqlite_createTable()


    with VideoTracker(cfg, args, video_path=args.VIDEO_PATH) as vdo_trk:
        vdo_trk.run()
    #print(jsonData)
    
    #print(dataOut)
    
    
    

    
    #jsonOutputPath = "output/json/"
    #outputFileName = jsonOutputPath + str(cameraNo) + "_" + videoStartTime_ori + ".txt"
    #file = open(outputFileName, 'w')
    #file.write(dataOut)
    #file.close()
    #test_draw_track()



