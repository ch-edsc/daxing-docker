import xml.etree.ElementTree as xml_tree
import os
from zipfile import ZipFile
zip_file=input("Drag the zip file to there and press enter : \n")
zip_file=zip_file.replace('\"','')
with ZipFile(zip_file, 'r') as zipObj:
  listOfFileNames = zipObj.namelist()
  for fileName in listOfFileNames:
    if fileName.endswith('.xml'):
      zipObj.extract(fileName)
xml_file = 'annotations.xml'
tree = xml_tree.parse(xml_file)
root = tree.getroot()
labels = {}
labels_node = root.find('meta').find('task').find('labels')

def get_number(item,tag):
  if(round(float(item.get(tag)),5)%1==0):
    try:
      return int(item.get(tag))
    except:
      return float(item.get(tag))   
  else:
    return float(item.get(tag))

def objects_overlap_area(possible_duplicate_object,current_track):
  if(possible_duplicate_object.get('label')==current_track.get('label')):
    current_box = current_track[0]
    predicted_box=predict_the_box(possible_duplicate_object)
    overlap_xtl = max(predicted_box[0],get_number(current_box,'xtl'))
    overlap_ytl = max(predicted_box[1],get_number(current_box,'ytl'))
    overlap_xbr = min(predicted_box[2],get_number(current_box,'xbr'))
    overlap_ybr = min(predicted_box[3],get_number(current_box,'ybr'))
    if( overlap_xtl < overlap_xbr ):
      if( overlap_ytl < overlap_ybr ):
        return ( overlap_xbr - overlap_xtl ) * ( overlap_ybr - overlap_ytl ) / ((get_number(current_box,'xbr') - get_number(current_box,'xtl')) * (get_number(current_box,'ybr') - get_number(current_box,'ytl'))) * 100
      else:
        return 0 
    else:
      return 0

def predict_the_box(track_object):
  box_2 = track_object[len(track_object)-2]
  if(len(track_object)-3<0):
    box_1 = box_2
  else:
    box_1 = track_object[len(track_object)-3]
  xtl = max(get_number(box_2,'xtl')*2 - get_number(box_1,'xtl'),0)
  ytl = max(get_number(box_2,'ytl')*2 - get_number(box_1,'ytl'),0)
  xbr = max(get_number(box_2,'xbr')*2 - get_number(box_1,'xbr'),0)
  ybr = max(get_number(box_2,'ybr')*2 - get_number(box_1,'ybr'),0)
  predicted_box = [xtl,ytl,xbr,ybr]
  return predicted_box

for label in labels_node.findall('label'):
  labels[label.find('name').text] = 0
labels_empty = labels.copy()
frame_max = 0
for track in root.findall('track'):
  for box in track.findall('box'):
    frame_max = get_number(box,'frame')

frame_max/=200;
frame_max=int(frame_max)
frame_max+=1;
possible_duplicate_objects = []

for i in range(1,frame_max+1):
  temp_object = possible_duplicate_objects.copy()
  possible_duplicate_objects = []
  labels = labels_empty.copy()
  for track in root:
    if( track.tag == "track" ):
      current_box = track.find('box')
      current_frame = get_number(current_box,'frame')
      last_box = track[len(track)-1]
      last_frame = get_number(last_box,'frame')
      if((last_frame-200*(i)==0) & (last_box.get('outside')=="1")):
        possible_duplicate_objects.append(track)
      if((current_frame<200*i) & (current_frame>=200*(i-1))):
        labels[track.get('label')] += 1
        if(current_frame==200*(i-1)):
          for track_object in temp_object:
            if(objects_overlap_area(track_object,track)!=None):
              if(objects_overlap_area(track_object,track)>60):
                labels[track.get('label')]-=1
                break    
  print(f'{200*(i-1)} - {200*i-1} frame\n')
  print(f'{labels} \n')
os.remove("annotations.xml")
os.system("pause")