# -*- coding: utf-8 -*-
"""
HW6_CNN分辨食物
"""

import numpy as np
import tensorflow as tf
import os
import sys
import cv2

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, Flatten, Dropout, BatchNormalization, AveragePooling2D
from tensorflow.keras.preprocessing import image

#讀圖出現Error， OSError: image file is truncated (73 bytes not processed)
#查 https://github.com/eriklindernoren/PyTorch-YOLOv3/issues/162
#那邊說需要加以下兩行
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

import random


class_num = 11
class_names = ['Bread','Dairy product','Dessert','Egg','Fried food',
               'Meat','Noodles/Pasta','Rice','Seafood','Soup',
               'Vegetable/Fruit']


img_W = 224
img_H = 224

random_seeds = 1
random.seed(random_seeds)
np.random.seed(random_seeds)
tf.random.set_seed(random_seeds)

'''
題目為李弘毅大師的教學網站，作業3
http://speech.ee.ntu.edu.tw/~tlkagk/courses_ML20.html


用 keras 的 ImageDataGenerator 來讀 data ，所以先手動把圖片依 ImageDataGenerator 的要求擺放

-----
課程內容說了多層CNN，前層後層的分別：
第一層，會分辨影像中的最基本元件(顏色，方向)，例如：影像中那些地方是紅色、是綠色、有特定方向條紋...等等
第二層~比較淺層，抓基本patten，例如：有綠色條紋、紅色圓形...等等
深層，會開始抓特定物件，例如車子的輪胎、鳥嘴、鳥的翅膀...等等
最後判斷類別，例如有鳥嘴、鳥翅膀，則判斷目標物體是鳥類


基本CNN結構： (conv -> maxpool)重複N次，然後 flatten 、通過全連接層輸出
-----
conv的卷積核是有深度的，深度與他的input相同，例如input 3*255*255 ，conv 5*5 ，實際上 conv 有深度3，卷積核是3*5*5
-----
cnn範例，一張圖是否為1個數字，有些前提假設(e.g. 筆畫有限)，做L1 norm
-----
Deep Dream ，把權重放大，讓CNN誇大化看到的東西
進階版：Deep Style，對圖A用CNN學content，對圖B用CNN學style，然後產生圖C，max化A的content與B的style
-----
CNN學圍棋，棋盤當成19x19的圖，黑子-1白子1空格0
-----
什麼時候可以用CNN ->有圖形的特性
(位置不重要但像素間相對位置重要、只要知道一個小範圍就可以預測、 ->所以可以 conv
 同樣patten出現在不同位置，代表同樣意義、 ->所以可以 conv
 可以subsampleing -> resize變縮圖後，還是保留原始信息)->所以可以 maxpool ->alpha go 下圍棋不能縮圖所以他沒有用maxpool
->CNN特別適合用在影像，如果用在下棋/聲音/文字要考慮特性來設計

======
loss: 0.1636 - accuracy: 0.9449 - val_loss: 1.3182 - val_accuracy: 0.7152
'''

def show(mat, name='___', scale = 1, pause = True):
    #運算過程的 show 圖放這邊，到時候要關掉直接把下面改 False 就好
    if True:
        if scale != 1:
            h, w = mat.shape
            scaleMat = cv2.resize(mat, (int(w * scale), int(h * scale)))
            cv2.imshow(name, scaleMat)
        else:
            scaleMat = cv2.resize(mat, (int(300), int(300)))
            cv2.imshow(name, mat)
        if pause :
            cv2.waitKey(0)  
    return

def exit(code=0):
    # OpenCV 結束前不把它產生的視窗關閉的話會 crash
    cv2.destroyAllWindows()
    sys.exit(code) 

def predict(imagePath, modelPath):
    #預測，參照 https://www.tensorflow.org/guide/keras/save_and_serialize
    model = tf.keras.models.load_model(modelPath)
    fileList = os.listdir(imagePath)
    for path in fileList:
        path = imagePath + path
        mat = cv2.imread(path)
        print(path)
        img = image.load_img(path, target_size=(img_W, img_H))
        x = image.img_to_array(img)
        x /= 255.0
        x = np.expand_dims(x, axis=0)
        y = model.predict_classes(x)
        tar = class_names[y[0]]
        print("%d %s" % (y[0], tar))
        show(mat, "image")
            

#這邊切換要訓練還是要預測
mode = 'train'
#mode = 'predict'

if __name__ == '__main__':

    if mode == 'train':
        
        #讀入圖片，方法參照Tensorflow官方說明頁面 https://www.tensorflow.org/api_docs/python/tf/keras/preprocessing/image/ImageDataGenerator
        #ImageDataGenerator 設定值參考 https://zhuanlan.zhihu.com/p/30197320
        '''
        train_datagen = ImageDataGenerator(
            rescale=1./255,
            rotation_range=30,
            channel_shift_range=5,
            zoom_range=0.1,
            horizontal_flip=True)
        '''
        train_datagen = ImageDataGenerator(
        rescale=1./255,
        rotation_range=180,
        channel_shift_range=15,
        zoom_range=0.3,
        shear_range=0.3,
        horizontal_flip=True)
        
        test_datagen = ImageDataGenerator(rescale=1./255)
        
        train_generator = train_datagen.flow_from_directory(
            'data/training',
            target_size=(img_W, img_H),
            batch_size=32,
            shuffle=True,
            class_mode='categorical')
        validation_generator = test_datagen.flow_from_directory(
            'data/validation',
            target_size=(img_W, img_H),
            batch_size=32,
            class_mode='categorical')
        
        #建立模型，基本的 CNN 模型，用 Conv / MaxPool / Relu / BN / Dropout 疊
        #valid 準確率 0.62，參考課程範例修改網路，BN層搬到pool前面，網路加到5層(雖然該範例準確率也才0.66沒高多少)
        n = 40 #模型大小用這個調整，控制 filters 數量
        lr = 0.00005
        epoch = 100
        model = Sequential()
        model.add(Conv2D(n, 3, activation='relu', padding='same', input_shape=[img_W, img_H, 3]))
        model.add(Conv2D(n, 3, activation='relu', padding='same'))
        model.add(Conv2D(n, 1, activation='relu', padding='same'))
        model.add(BatchNormalization())
        model.add(MaxPool2D(pool_size=(2,2)))
        
        
        model.add(Conv2D(n*2, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*2, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*2, 1, activation='relu', padding='same'))
        model.add(BatchNormalization())
        model.add(MaxPool2D(pool_size=(2,2)))
        
        #model.add(Dropout(0.5))
        
        model.add(Conv2D(n*4, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*4, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*4, 1, activation='relu', padding='same'))
        model.add(BatchNormalization())
        model.add(MaxPool2D(pool_size=(2,2)))
        
        #model.add(Dropout(0.5))
        
        model.add(Conv2D(n*8, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*8, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*8, 1, activation='relu', padding='same'))
        model.add(BatchNormalization())
        model.add(MaxPool2D(pool_size=(2,2)))
        #model.add(Dropout(0.5))
        
        model.add(Conv2D(n*8, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*8, 3, activation='relu', padding='same'))
        model.add(Conv2D(n*8, 1, activation='relu', padding='same'))
        model.add(BatchNormalization())
        model.add(MaxPool2D(pool_size=(2,2)))
        
        
        model.add(Flatten())
        #model.add(Dropout(0.5))
        #model.add(BatchNormalization()) #查了一些資料，有人說 BN 與 DropOut 不要一起用，見 https://arxiv.org/abs/1801.05134
        model.add(Dense(1024, activation='relu'))
        model.add(Dense(class_num, activation='softmax'))
        
        model.summary()   
        
        opt = tf.keras.optimizers.Adam(learning_rate=lr)
        model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    
        #開始 train
        model.fit(
            train_generator,
            epochs=epoch,
            validation_data=validation_generator)
        model.save('model.h5')
        
        #然後預測
        predictModelPath = "model.h5"
        predict("data/testing/", "model.h5")
    elif mode == 'predict':
        #這邊是不train，直接讀入model預測
        predictModelPath = "model.h5"
        predict("data/training/10/", "model.h5")
        #predict("data/testing/", "model.h5")
    exit()