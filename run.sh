#!/usr/bin/env bash
docker-compose build
docker-compose -f docker-compose.yml -f components/serverless/docker-compose.serverless.yml up -d
docker exec -it cvat bash -ic "echo \"from django.contrib.auth.models import User; User.objects.filter(username='admin').delete(); User.objects.create_superuser('admin', 'no email', '123')\" | python manage.py shell"
