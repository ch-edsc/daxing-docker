#
# Copyright (C) 2018-2021 Intel Corporation
#
# SPDX-License-Identifier: MIT
#
version: '3.3'

services:

  notebook:
    container_name: jupyter_notebook_cuda
    runtime: nvidia
    build:
      context: ./jupyter-notebook
    ports:
      - "8888:8888"
      - "6006:6006"
    depends_on:
      - mlflow
    networks:
      - default
    restart: always
    environment:
      MLFLOW_TRACKING_URI : "http://mlflow:5000"
      AWS_ACCESS_KEY_ID : ${AWS_ACCESS_KEY_ID}
      AWS_SECRET_ACCESS_KEY : ${AWS_SECRET_ACCESS_KEY}
      MLFLOW_S3_ENDPOINT_URL: "http://s3:9000"
      JUPYTER_ENABLE_LAB : "yes"
      NVIDIA_VISIBLE_DEVICES: "all"
    volumes:
      - $HOME/:/home/daxing/work
##??

  s3:
    image: minio/minio:RELEASE.2021-07-08T19-43-25Z
    container_name: aws-s3
    ports:
      - 9090:9090
      - 9000:9000
    environment:
      - MINIO_ACCESS_KEY=${AWS_ACCESS_KEY_ID}
      - MINIO_SECRET_KEY=${AWS_SECRET_ACCESS_KEY}
    command: server /date --console-address ":9090"
    networks:
      - default
    restart: always
    volumes:
      - ./s3:/date

  db:
      restart: always
      image: mysql/mysql-server:5.7.28
      container_name: mlflow_db
      expose:
          - "3306"
      environment:
          - MYSQL_DATABASE=${MYSQL_DATABASE}
          - MYSQL_USER=${MYSQL_USER}
          - MYSQL_PASSWORD=${MYSQL_PASSWORD}
          - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
      volumes:
          - ./dbdata:/var/lib/mysql
      networks:
          - default
      restart: always

  mlflow:
      container_name: tracker_mlflow
      image: tracker_ml
      build:
          context: ./mlflow
          dockerfile: Dockerfile
      ports:
          - "5000:5000"
      environment: 
          - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
          - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
          - AWS_DEFAULT_REGION=${AWS_REGION}
          - MLFLOW_S3_ENDPOINT_URL=http://s3:9000
      networks:
          - default
      entrypoint: ./wait-for-it.sh db:3306 -t 90 -- mlflow server --backend-store-uri mysql+pymysql://${MYSQL_USER}:${MYSQL_PASSWORD}@db:3306/${MYSQL_DATABASE} --default-artifact-root s3://${AWS_BUCKET_NAME}/ -h 0.0.0.0

  cvat_db:
    container_name: cvat_db
    image: postgres:10-alpine
    networks:
      default:
        aliases:
          - db
    restart: always
    environment:
      POSTGRES_USER: root
      POSTGRES_DB: cvat
      POSTGRES_HOST_AUTH_METHOD: trust
    volumes:
      - cvat_db:/var/lib/postgresql/data

  cvat_redis:
    container_name: cvat_redis
    image: redis:4.0-alpine
    networks:
      default:
        aliases:
          - redis
    restart: always

  cvat:
    container_name: cvat
    image: openvino/cvat_server
    restart: always
    depends_on:
      - cvat_redis
      - cvat_db
    environment:
      DJANGO_MODWSGI_EXTRA_ARGS: ''
      ALLOWED_HOSTS: '*'
      CVAT_REDIS_HOST: 'cvat_redis'
      CVAT_POSTGRES_HOST: 'cvat_db'
      ADAPTIVE_AUTO_ANNOTATION: 'false'
    volumes:
      - cvat_data:/home/django/data
      - cvat_keys:/home/django/keys
      - cvat_logs:/home/django/logs


  cvat_ui:
    container_name: cvat_ui
    image: openvino/cvat_ui
    restart: always
    networks:
      default:
        aliases:
          - ui
    depends_on:
      - cvat

  cvat_proxy:
    container_name: cvat_proxy
    image: nginx:stable-alpine
    restart: always
    depends_on:
      - cvat
      - cvat_ui
    environment:
      CVAT_HOST: localhost 140.120.56.240 127.0.0.1
    ports:
      - '8080:80'
    volumes:
      - ./cvat_proxy/nginx.conf:/etc/nginx/nginx.conf:ro
      - ./cvat_proxy/conf.d/cvat.conf.template:/etc/nginx/conf.d/cvat.conf.template:ro
    command: /bin/sh -c "envsubst '$$CVAT_HOST' < /etc/nginx/conf.d/cvat.conf.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"




networks:
  default:
    driver: bridge

volumes:
  cvat_db:
  cvat_data:
  cvat_keys:
  cvat_logs:
